﻿using System.Collections.Generic;
using System.Linq;
using DateTimeBombs.NodaTimeDateTimeProvider.Groceries;
using NodaTime;

namespace DateTimeBombs.NodaTimeDateTimeProvider
{
    public class Fridge
    {
        public List<IGroceryItem> GroceryItems = new List<IGroceryItem>();
        private readonly IClock _clock;
        private LocalDate _getCurrentDate => new ZonedClock(_clock, DateTimeZone.Utc, CalendarSystem.Gregorian).GetCurrentDate();
        
        public Fridge(IClock clock)
        {
            _clock = clock;
        }

        public void StockFridge(IEnumerable<IGroceryItem> groceryItems)
        {
            GroceryItems.AddRange(groceryItems);
        }

        public bool IsTimeToCleanTheFridge()
        {
            return GroceryItems
                .Any(g => g.ExpirationDate < _getCurrentDate);
        }

        public int NumberOfExpiredItems()
        {
            return GroceryItems
                .Count(g => g.ExpirationDate < _getCurrentDate);
        }
    }
}
