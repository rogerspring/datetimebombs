﻿using NodaTime;

namespace DateTimeBombs.NodaTimeDateTimeProvider.Groceries
{
    public class Milk : GroceryItem, IGroceryItem
    {
        private readonly Period _shelfLife = Period.FromDays(7);

        public LocalDate ExpirationDate => PurchaseDate.Plus(_shelfLife);
        
        public Milk(LocalDate purchaseDate)
        {
            PurchaseDate = purchaseDate;
        }
    }
}
