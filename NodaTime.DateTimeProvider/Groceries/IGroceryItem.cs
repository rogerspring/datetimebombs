﻿using NodaTime;

namespace DateTimeBombs.NodaTimeDateTimeProvider.Groceries
{
    public interface IGroceryItem
    {
        LocalDate ExpirationDate { get; }
    }
}