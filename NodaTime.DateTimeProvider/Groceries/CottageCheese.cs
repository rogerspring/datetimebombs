﻿using NodaTime;

namespace DateTimeBombs.NodaTimeDateTimeProvider.Groceries
{
    public class CottageCheese : GroceryItem, IGroceryItem
    {
        private readonly Period _shelfLife = Period.FromDays(10);

        public LocalDate ExpirationDate => PurchaseDate.Plus(_shelfLife);
        
        public CottageCheese(LocalDate purchaseDate)
        {
            PurchaseDate = purchaseDate;
        }
    }
}
