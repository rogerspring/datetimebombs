﻿using NodaTime;

namespace DateTimeBombs.NodaTimeDateTimeProvider.Groceries
{
    public class Ketchup : GroceryItem, IGroceryItem
    {
        private readonly Period _shelfLife = Period.FromDays(182);

        public LocalDate ExpirationDate => PurchaseDate.Plus(_shelfLife);
        
        public Ketchup(LocalDate purchaseDate)
        {
            PurchaseDate = purchaseDate;
        }

    }
}
