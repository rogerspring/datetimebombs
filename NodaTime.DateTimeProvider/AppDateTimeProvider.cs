﻿using NodaTime;

namespace DateTimeBombs.NodaTimeDateTimeProvider
{
    public class AppDateTimeProvider : IClock
    {
        public Instant GetCurrentInstant()
        {
            return SystemClock.Instance.GetCurrentInstant();
        }
    }
}
