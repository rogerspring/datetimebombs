﻿

/*** This attribute is only necessary if AssemblyInfo.cs is not included in project declaring the same the whole assembly ****/
// using System.Runtime.CompilerServices;
// [assembly: InternalsVisibleTo("DateTimeBombs.NodaTimeDateTimeProvider.Tests")]

using System.Collections.Generic;

namespace DateTimeBombs.NodaTimeDateTimeProvider
{
    internal class ExampleInternal
    {
        internal int Add()
        {
            return 1 + 1;
        }
    }
}
