using FluentAssertions;
using FluentAssertions.Execution;
using DateTimeBombs.NodaTimeDateTimeProvider.Groceries;
using NodaTime;
using NodaTime.Testing;
using Xunit;
using Xunit.Abstractions;

namespace DateTimeBombs.NodaTimeDateTimeProvider.Tests
{
    public class FridgeTests
    {
        private readonly ITestOutputHelper _outputHelper;

        public FridgeTests(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public void IsTimeToCleanTheFridge_WithExpiredItems_ReturnsTrue()
        {
            FakeClock fakeClock = FakeClock.FromUtc(2019, 01, 01);
            var today = GetDate(fakeClock);

            var sut = new Fridge(fakeClock);

            sut.StockFridge(new IGroceryItem[]
            {
                new CottageCheese(today),
                new Milk(today),
                new Ketchup(today)
            });

            using (new AssertionScope())
            {
                sut.IsTimeToCleanTheFridge().Should().BeFalse();

                fakeClock.Advance(Duration.FromDays(5));
                sut.IsTimeToCleanTheFridge().Should().BeFalse();

                fakeClock.Advance(Duration.FromDays(5));
                sut.IsTimeToCleanTheFridge().Should().BeTrue();
            }

        }        
        
        [Fact]
        public void NumberOfExpiredItems_WithExpiredItems_ReturnsCorrectCount()
        {
            var fakeClock = FakeClock.FromUtc(2019, 01, 01);
            var today = GetDate(fakeClock);

            var sut = new Fridge(fakeClock);

            sut.StockFridge(new IGroceryItem[]
            {
                new CottageCheese(today),
                new Milk(today),
                new Ketchup(today)
            });

            using (new AssertionScope())
            {
                sut.NumberOfExpiredItems().Should().Be(0);

                fakeClock.Advance(Duration.FromDays(9));
                _outputHelper.WriteLine($"Days since stocked fridge {Period.Between(GetDate(fakeClock), today, PeriodUnits.Days)}");
                _outputHelper.WriteLine($"Number Of Expired Items: {sut.NumberOfExpiredItems()}");
                sut.NumberOfExpiredItems().Should().Be(1);

                fakeClock.Advance(Duration.FromDays(3));
                _outputHelper.WriteLine($"Days since stocked fridge {Period.Between(GetDate(fakeClock), today, PeriodUnits.Days)}");
                _outputHelper.WriteLine($"Number Of Expired Items: {sut.NumberOfExpiredItems()}");
                sut.NumberOfExpiredItems().Should().Be(2);

                fakeClock.Advance(Duration.FromDays(170));
                _outputHelper.WriteLine($"Days since stocked fridge {Period.Between(GetDate(fakeClock), today, PeriodUnits.Days)}");
                _outputHelper.WriteLine($"Number Of Expired Items: {sut.NumberOfExpiredItems()}");
                sut.NumberOfExpiredItems().Should().Be(2);

                fakeClock.Advance(Duration.FromDays(1));
                _outputHelper.WriteLine($"Days since stocked fridge {Period.Between(GetDate(fakeClock), today, PeriodUnits.Days)}");
                _outputHelper.WriteLine($"Number Of Expired Items: {sut.NumberOfExpiredItems()}");
                sut.NumberOfExpiredItems().Should().Be(3);
            }
        }

        private static LocalDate GetDate(FakeClock fakeClock)
        {
            return new ZonedClock(fakeClock, DateTimeZone.Utc, CalendarSystem.Gregorian).GetCurrentDate();
        }
    }
}