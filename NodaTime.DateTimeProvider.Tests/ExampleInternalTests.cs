using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using DateTimeBombs.NodaTimeDateTimeProvider;
using FluentAssertions;
using NodaTime;

namespace DateTimeBombs.NodaTimeDateTimeProvider.Tests
{
    [TestFixture]
    public class ExampleInternalTests
    {
        [Test]
        public void TestMethod1()
        {
            // Arrange
            var internalTests = new ExampleInternal();

            // Act
            var actual = internalTests.Add();

            // Assert
            Assert.That(actual, Is.EqualTo(2));
        }
        [Test]
        public void CanParseMinutes()
        {
            var p = Period.FromDays(30);
            Console.WriteLine(p.ToString());
        }


        [Test]
        public void TrickyMonthArithmetic()
        {
            DateTime d;
            LocalDate nd;

            /* On further inspection, this makes more sense. Months can be tricky. Years can be tricky too, it's part of the
             * reason TimeSpans don't include either of them. The example, with 2/29 being involved might lead someone to
             * believe this is strictly a leap year issue, but it isn't.
             */

            d = new DateTime(2020, 3, 31).AddMonths(2);
            Console.WriteLine(d);
            // 5/31/2020

            /* When adding a 'month' to a 3/31, you will get 4/30 because the idea of adding a month should be a date
             * in April sometime, but since 4/31 is invalid, it falls back to 4/30. 
             */
            d = new DateTime(2020, 3, 31).AddMonths(1);
            Console.WriteLine(d);
            // 4/30/2020

            /* Now add a month to that and you're not asking the method to give you the next end of month date, you're
             * asking for a 'month' of days to be added, which gets you 5/30.
             */

            // d = 4/30/2020
            d = d.AddMonths(1);
            Console.WriteLine(d);
            // 5/30/2020


            
            /* Do the same starting with 4/30, adding one month, you'll get 5/30. Add another month and you'll get 6/30. */
            d = new DateTime(2020, 6, 30).AddMonths(1);
            Console.WriteLine(d);
            // 7/30/2020

            /* Because falling back a day isn't necessary, you'll get the same date when adding two months in one call. */
            // d = 7/30/2020
            d = d.AddMonths(1);
            Console.WriteLine(d);
            // d = 8/30/2020

            d = new DateTime(2020, 6, 30).AddMonths(2);
            Console.WriteLine(d);
            // d = 8/30/2020

            /*
             * Consider the following as another example of the same issue.
             * If you add a 'month' to 4/30, you'll get 5/30. Add another month
             */
            // Using BCL DateTime
            d = new DateTime(2020, 4, 30).AddMonths(1).AddMonths(1).AddMonths(1);
            Console.WriteLine(d);
            d = new DateTime(2020, 4, 30).AddMonths(3);
            Console.WriteLine(d);

            d = new DateTime(2020, 3, 31).AddMonths(1).AddMonths(1);
            Console.WriteLine(d);
            d = new DateTime(2020, 3, 31).AddMonths(2);
            Console.WriteLine(d);

            // Using NodaTime LocalDate
            nd = new LocalDate(2020, 1, 31).PlusMonths(1).PlusMonths(1);
            Console.WriteLine(nd);
            nd = new LocalDate(2020, 1, 31).PlusMonths(2);
            Console.WriteLine(nd);

            nd = new LocalDate(2020, 3, 31).PlusMonths(1).PlusMonths(1);
            Console.WriteLine(nd);
            nd = new LocalDate(2020, 3, 31).PlusMonths(2);
            Console.WriteLine(nd);


            // Non Leap year
            d = new DateTime(2019, 1, 31).AddMonths(1).AddMonths(1);
            Console.WriteLine(d);
            d = new DateTime(2019, 1, 31).AddMonths(2);
            Console.WriteLine(d);

            d = new DateTime(2019, 1, 31).AddMonths(1).AddMonths(1);
            Console.WriteLine(d);
            d = new DateTime(2019, 1, 31).AddMonths(2);
            Console.WriteLine(d);

            Assert.That(new DateTime(2020, 4, 30).AddMonths(1).AddMonths(1) == new DateTime(2020, 4, 30).AddMonths(2));
            Assert.That(new DateTime(2020, 5, 31).AddMonths(1) == new DateTime(2020, 5, 30).AddMonths(1));


        }

        [Test]
        public void OperatorTests()
        {
            //var d1 = new OffsetDateTime(new LocalDateTime(2020, 04, 28, 0, 0, 0), Offset.FromHours(-8));
            //var d2 = new OffsetDateTime(new LocalDateTime(2020, 04, 29, 0, 0, 0), Offset.FromHours(-8));
            var d1 = Instant.FromDateTimeOffset(new DateTimeOffset(2020, 04, 28, 0, 0, 0, TimeSpan.FromHours(-8)));
            var d2 = Instant.FromDateTimeOffset(new DateTimeOffset(2020, 04, 29, 0, 0, 0, TimeSpan.FromHours(-8)));

            bool actual = d1 < d2;

            actual.Should().BeTrue();
        }

        [Test]
        public void InstantTesting()
        {
            var i = SystemClock.Instance.GetCurrentInstant();
            var u = i.ToUnixTimeSeconds().ToString();
            Console.WriteLine(u);
            // u = "1608305407"

            var i2 = Instant.FromUnixTimeSeconds(long.Parse(u));
            Console.WriteLine(i2.ToUnixTimeSeconds());
            // 1608305407

        }
    }
}
