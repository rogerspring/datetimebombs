import { Component, OnInit } from '@angular/core';
import { DatesClient, MyDto, ICalendarDay } from '../api/api';
@Component({
  selector: 'app-dates-on-api',
  templateUrl: './dates-on-api.component.html',
  styleUrls: ['./dates-on-api.component.css']
})
export class DatesOnApiComponent implements OnInit {

  today: MyDto;

  constructor(
    private dateClient: DatesClient,
    private datesClient: DatesClient
  ) { }

  ngOnInit() {

    this.dateClient.getTodayAsCalendarDay().subscribe(
      (d: ICalendarDay) => {
        console.debug('Day', d.day);
        console.debug('Month', d.month);
        console.debug('Year', d.year);
      });

    // this.dateTypesClient.getLocalDate()
    //   .subscribe((d) => {
    //     const x = d.day;

    //     console.debug(d);
    //   });
  }

}
