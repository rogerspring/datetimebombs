import * as moment from 'moment';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApiService } from './api.service';
import { debounceTime, switchMap, tap, catchError, finalize } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { throwError } from 'rxjs';
import { DatesClient } from './api/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  dates: FormGroup = this.fb.group({
    htmlDate: [''],
    matDate: [''],
    textDate: ['']
  });

  textDateInterpreted = ' ';
  textDateValue: moment.Moment | Date | string = ' ';
  htmlDateInterpreted: Date | string = ' ';
  htmlDateValue: Date | string = ' ';
  matDateInterpreted: moment.Moment | string = ' ';
  matDateValue: moment.Moment | Date | string = ' ';

  constructor(private api: ApiService,
    private readonly dateClient: DatesClient,
    private fb: FormBuilder) { }

  /*
   // Add your code here
  import * as moment from 'moment';
  
  
  const mainDiv = document.getElementById('main');
  
  
  // **** Leaps forward **** //
  moment('2020-02-29T00:00:00Z').toISOString();
  // 2020-02-29T00:00:00.000Z
  moment('2020-02-29T00:00:00Z').add(1, 'year').toISOString()
  // 2021-03-01T00:00:00.000Z
  
  mainDiv.innerHTML += `<h1>${moment('2020-02-29T00:00:00Z').toISOString()}</h1>`;
  mainDiv.innerHTML += `<p>${moment('2020-02-29T00:00:00Z').add(1, 'year').toISOString()}</p>`;
  
  // **** Falls back **** //
  moment.utc('2020-02-29T00:00:00Z').toISOString();
  // 2020-02-29T00:00:00.000Z
  moment.utc('2020-02-29T00:00:00Z').add(1, 'year').toISOString()
  // 2021-02-28T00:00:00.000Z
  
  mainDiv.innerHTML += `<h1>${moment.utc('2020-02-29T00:00:00Z').toISOString()}</h1>`;
  mainDiv.innerHTML += `<p>${moment.utc('2020-02-29T00:00:00Z').add(1, 'year').toISOString()}</p>`;
   */

  ngOnInit() {
    // console.log('datepicker value initialized as', this.fcDate.value);
    this.setupDtoDateListeners();
  }
  setupDtoDateListeners() {
    this.dates
      .get('textDate')
      .valueChanges.pipe(
        debounceTime(1000),
        // tap(() => {
        //   console.debug('Calling getDate');
        // }),
        // switchMap((t: string) => {
        //   return this.api.getDate(t);
        // }),
        // catchError(err => {
        //   console.debug('throwing error');
        //   return throwError(err);
        // }),
        // finalize(() => {
        //   console.debug('finalizing');
        // }),
        tap((v) => {
          console.debug(`getDate returned ${v}`);
        })
      )
      .subscribe((dt: string) => {
        if (!dt) {
          return;
        }

        this.textDateInterpreted = dt;
        this.textDateValue = new Date(dt).toString();
        // this.api.saveDate(dt);
      });

    this.dates
      .get('htmlDate')
      .valueChanges.pipe(debounceTime(1000))
      .subscribe((dt: Date) => {
        if (!dt) {
          return;
        }

        this.htmlDateInterpreted = dt;
        this.htmlDateValue = new Date(dt).toString();
        // this.api.saveDate(dt);
      });

    this.dates
      .get('matDate')
      .valueChanges.pipe(debounceTime(1000))
      .subscribe((dt: moment.Moment | string) => {
        if (!dt) {
          return;
        }

        this.matDateInterpreted = moment.isMoment(dt) ? 'moment' : dt;

        if (moment.isMoment(dt)) {
          this.matDateValue = moment(dt).toString();
          this.dateClient.getSerialize(dt.toJSON()).subscribe(r =>
            // tslint:disable-next-line:no-console
            console.log(r)
          )
        } else {
          const payload = new Date(dt).toJSON();
          this.matDateValue = payload;
          this.dateClient.getSerialize(payload).subscribe(r =>
            // tslint:disable-next-line:no-console
            console.log(r)
          )
        }
      });
  }
}
