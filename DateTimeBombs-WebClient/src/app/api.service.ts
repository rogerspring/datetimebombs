import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
	private http: HttpClient;

	baseURL = 'https://localhost:44309';
	constructor(@Inject(HttpClient) http: HttpClient) {
		this.http = http;
	}

	getDate(d: string): Observable<string> {
		const date = `date=${encodeURIComponent('' + d)}`;

		return this.http.get(`${this.baseURL}/cancelable/?${date}`).pipe(
				map(r => r.toString()
			)
		);
	}

  // constructor(private http: HttpClient) { }

  saveDate(dt: moment.Moment | string ) {
    if (moment.isMoment(dt)) {
      console.info('Recieved Moment: ', moment(dt).toString());
    } else {
      console.info('Recieved Moment: ', new Date(dt).toString());
    }
    // this.http.post('http://')
  }
}
