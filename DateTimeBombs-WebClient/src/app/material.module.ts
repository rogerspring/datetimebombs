import { NgModule } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {
	MAT_MOMENT_DATE_ADAPTER_OPTIONS,
	MAT_MOMENT_DATE_FORMATS,
	MatMomentDateModule,
	MomentDateAdapter
} from '@angular/material-moment-adapter';
import {
	DateAdapter,
	MAT_DATE_FORMATS,
	MAT_DATE_LOCALE
} from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSortModule } from '@angular/material/sort';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatListModule } from '@angular/material/list';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSliderModule } from '@angular/material/slider';
import { MatTreeModule } from '@angular/material/tree';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatBadgeModule } from '@angular/material/badge';
import { MatExpansionModule } from '@angular/material/expansion';

const modules = [MatCheckboxModule,
	MatFormFieldModule,
	MatInputModule,
	MatSelectModule,
	MatTabsModule,
	MatToolbarModule,
	MatButtonModule,
	MatCardModule,
	MatGridListModule,
	MatIconModule,
	MatMenuModule,
	MatSnackBarModule,
	MatPaginatorModule,
	MatTableModule,
	MatAutocompleteModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatChipsModule,
	MatDatepickerModule,
	MatTooltipModule,
	MatSortModule,
	MatSidenavModule,
	MatButtonToggleModule,
	MatBadgeModule,
	MatRadioModule,
	MatListModule,
	MatSliderModule,
	MatSlideToggleModule,
	MatNativeDateModule,
	MatSlideToggleModule,
	MatBadgeModule,
	MatExpansionModule,
	MatBottomSheetModule,
	MatDialogModule,
	MatTreeModule]
// https://material.angular.io/components/categories
@NgModule({
	imports: [
		...modules
	],
	exports: [
		...modules
	],
	// providers: [
	// 	MatDatepickerModule,
	// 	/* https://material.angular.io/components/datepicker/overview#choosing-a-date-implementation-and-date-format-settings
	// 	 * By default the MomentDateAdapter will creates dates in your time zone specific locale.
	// 	 * You can change the default behaviour to parse dates as UTC by providing the
	// 	 * MAT_MOMENT_DATE_ADAPTER_OPTIONS and setting it to useUtc: true.
	// 	 */
	// 	// {
	// 	// 	provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
	// 	// 	useValue: { useUtc: true }
	// 	// },
	// 	// {
	// 	// 	provide: DateAdapter,
	// 	// 	useClass: MomentDateAdapter,
	// 	// 	deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
	// 	// },
	// 	// { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
	// ]
})
export class MaterialModule { }
