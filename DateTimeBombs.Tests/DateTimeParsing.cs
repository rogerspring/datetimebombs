﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace DateTimeBombs.Tests
{
    [TestFixture]
    public class DateTimeParsing
    {
        [Test]
        public void TryParseExactUnspecified()
        {
            DateTime dt = DateTime.Now;
            Console.WriteLine(dt.ToString("yyyy-MM-dd'T'HH:mm:ss.FFFFFFF"));
            Console.WriteLine(dt.ToString("yyyy-MM-dd'T'HH:mm:ss.FFFFFFFZ"));
            Console.WriteLine(dt.ToString("yyyy-MM-dd'T'HH:mm:ss.FFFFFFFzzz"));

            string stringValue = "01-15-2019 00:00:00";
            string[] DateTimeFormats =
                {"yyyyMMdd'T'HHmmss.FFFFFFFK", "yyyy-MM-dd'T'HH:mm:ss.FFFFFFFK", "MM-dd-yyyy HH:mm:ss"};

            var succeeded = DateTime.TryParseExact(stringValue, DateTimeFormats, CultureInfo.InvariantCulture,
                DateTimeStyles.RoundtripKind, out var result);

            Assert.That(succeeded, Is.True);
            Assert.That(result.Kind, Is.EqualTo(DateTimeKind.Unspecified));

        }

        [Test]
        public void TryParseExactUtc()
        {
            string stringValue = "2019-01-15T00:00:00Z";
            string[] DateTimeFormats =
                {"yyyyMMdd'T'HHmmss.FFFFFFFK", "yyyy-MM-dd'T'HH:mm:ss.FFFFFFFK"};

            var succeeded = DateTime.TryParseExact(stringValue, DateTimeFormats, CultureInfo.InvariantCulture,
                DateTimeStyles.RoundtripKind, out var result);

            Assert.That(succeeded, Is.True);
            Assert.That(result.Kind, Is.EqualTo(DateTimeKind.Utc));

        }

        [TestCase("01-15-2019")]
        [TestCase("01/15/2019")]
        [TestCase("2019-01-15")]
        [TestCase("2019/01/15")]
        public void ParseCalendarDay(string input)
        {
            string pattern =
                @"(?<month>\d{2})[-\/](?<day>\d{2})[-\/](?<year>\d{4})|(?<year>\d{4})[-\/](?<month>\d{2})[-\/](?<day>\d{2})";

            RegexOptions options = RegexOptions.Multiline;

            var matches = Regex.Matches(input, pattern, options);

            var strMonth = matches[0].Groups["month"].ToString();
            var strDay = matches[0].Groups["day"].ToString();
            var strYear = matches[0].Groups["year"].ToString();
            Assert.That(strMonth, Is.EqualTo("01"));
            Assert.That(strDay, Is.EqualTo("15"));
            Assert.That(strYear, Is.EqualTo("2019"));
        }


        [Test]
        public void JObjectParseFailure()
        {
            var json = 
                @"{
                    'company': 'AltSource Inc.',
                    'time':'2016-08-01T03:04:05Z',
                    'city': 'Portland'
                }";

            var jObject = JObject.Parse(json);

            var company = jObject.Value<string>("company");
            Assert.That(company, Is.EqualTo("AltSource Inc."));

            var time = jObject.Value<string>("time");

            // This will fail, unless we configure DateParseHandling
            Assert.That(time, Is.Not.EqualTo("2016-08-01T03:04:05Z")); 
        }

        [Test]
        public void JTokenParse_DateParseHandling_ImpliedOffset()
        {
            var json = 
                @"{
                    'company': 'AltSource Inc.',
                    'time':'2016-08-01T03:04:05',
                    'city': 'Portland'
                }";

            using (var reader = new StringReader(json))
            {

                JsonReader jsonReader = new JsonTextReader(reader)
                {
                    //DateFormatString = "yyyy-dd-MM",
                    DateParseHandling = DateParseHandling.DateTimeOffset,
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc
                };

                var jObject = JObject.Load(jsonReader);
                var actual = jObject.Value<DateTimeOffset>("time");
                var expected = new DateTimeOffset(new DateTime(2016, 08, 01, 03, 04, 05), new TimeSpan(0, 0, 0));

                Assert.That(actual, Is.Not.EqualTo(expected));
            }
        }

        [Test]
        public void JTokenParse_DateParseHandling_DateTime_Utc()
        {
            //Ending string makes it Utc
            using (var reader = new StringReader("{'myStringDate':'2016-08-01T03:04:05Z'}"))
            {

                JsonReader jsonReader = new JsonTextReader(reader)
                {
                    DateParseHandling = DateParseHandling.DateTime
                };

                var jObject = JObject.Load(jsonReader);
                var actual = jObject.Value<DateTime>("myStringDate");
                var expected = DateTime.SpecifyKind(new DateTime(2016, 08, 01, 03, 04, 05), DateTimeKind.Utc);
                
                Assert.That(actual, Is.EqualTo(expected));
                Assert.That(actual.Kind, Is.EqualTo(expected.Kind));
            }
        }


        [Test]
        public void JTokenParse_DateParseHandling_DateTime_Unspecified()
        {
            //Since string does not end with Z, it's unspecified
            using (var reader = new StringReader("{'myStringDate':'2016-08-01T03:04:05'}"))
            {

                JsonReader jsonReader = new JsonTextReader(reader)
                {
                    DateParseHandling = DateParseHandling.DateTime
                };

                var jObject = JObject.Load(jsonReader);
                var actual = jObject.Value<DateTime>("myStringDate");
                var expected = DateTime.SpecifyKind(new DateTime(2016, 08, 01, 03, 04, 05), DateTimeKind.Unspecified);
                
                Assert.That(actual, Is.EqualTo(expected));
                Assert.That(actual.Kind, Is.EqualTo(expected.Kind));
            }
        }

        [Test]
        public void JObjectParse_DateParseHandling_None()
        {
            using (var reader = new StringReader("{'myStringDate':'2016-08-01T03:04:05Z'}"))
            {

                JsonReader jsonReader = new JsonTextReader(reader)
                {
                    DateParseHandling = DateParseHandling.None
                };

                var jObject = JObject.Load(jsonReader);
                var actual = jObject.Value<string>("myStringDate");
                
                Assert.That(actual, Is.EqualTo("2016-08-01T03:04:05Z"));
            }
        }
    }
}
