﻿using System;
using System.Linq;
using NodaTime;
using NodaTime.Text;
using NUnit.Framework;
using TimeZoneNames;
using static System.FormattableString;

namespace DateTimeBombs.Tests
{
    [TestFixture]
    public class TimeZoneInfoTests
    {
        [Test]
        public void CultureInvariant()
        {
            // using static System.FormattableString;
            // provides the Invariant("formatted string here") syntax
            DateTimeOffset timestamp = new DateTimeOffset(new DateTime(2019, 1, 1), TimeSpan.Zero);

            var stringStamp = Invariant($"{timestamp:yyyy-MM-dd'T'HH:mm:sszzz} Logged");
            Console.WriteLine(stringStamp);
            Assert.That(stringStamp, Is.EqualTo("2019-01-01T00:00:00+00:00 Logged"));
        }

        [Test]
        public void TimeZoneInfoVsNodaTime()
        {
            DateTimeOffset msFeb1974 = new DateTimeOffset(new DateTime(1974, 1, 5, 12, 0, 0), new TimeSpan(-5, 0, 0));
            //Note the 'timezone' id here is actually worded as an offset id.
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            var easternTime = TimeZoneInfo.ConvertTime(msFeb1974, easternZone);

            //This is incorrect
            Assert.That(TimeZoneInfo.Local.IsDaylightSavingTime(msFeb1974), Is.False);

            Console.WriteLine("{0: yyyy-MM-ddTHH:mm:ssZ} {1} corresponds to {2: yyyy-MM-ddTHH:mm:ssZ} {3}.", msFeb1974, 
                TimeZoneInfo.Local.IsDaylightSavingTime(msFeb1974) 
                    ? TimeZoneInfo.Local.DaylightName 
                    : TimeZoneInfo.Local.StandardName, 
                easternTime, easternZone.IsDaylightSavingTime(easternTime) 
                    ? easternZone.DaylightName 
                    : easternZone.StandardName);


            var timezone = "America/New_York";
            var nodaFeb1974 = new LocalDateTime(1974, 1, 5, 12, 0, 0);
            var names = TZNames.GetNamesForTimeZone(timezone, "en-US");

            var zone = DateTimeZoneProviders.Tzdb[timezone];
            var easternDateTime = new ZonedDateTime(nodaFeb1974, zone, Offset.FromHours(-5));
            //Assert.That(easternDateTime.IsDaylightSavingTime(), Is.True);
            
            var pattern = ZonedDateTimePattern.ExtendedFormatOnlyIso;
            Console.WriteLine("{0: yyyy-MM-ddTHH:mm:ss} Local corresponds to {1: yyyy-MM-ddTHH:mm:ss} {2}.", 
                nodaFeb1974, 
                easternTime, 
                easternDateTime.IsDaylightSavingTime() 
                    ? easternZone.DaylightName 
                    : easternZone.StandardName);
        }

        [Test]
        public void TimeZoneInfoAndDateTimeKind()
        {
            //https://codeofmatt.com/beware-the-edge-cases-of-time/
            var tzName = "Eastern Standard Time";

            var dtBeforePDT = new DateTime(2015, 3, 8, 1, 0, 0);
            Console.WriteLine(dtBeforePDT.Kind);
            Console.WriteLine("{0}, is {1} off of UTC, with respect to {2:o} of {3} kind.",
                tzName,
                TimeZoneInfo.FindSystemTimeZoneById(tzName)
                .GetUtcOffset(dtBeforePDT),
                dtBeforePDT, 
                dtBeforePDT.Kind);

            var dtAfterPDT = new DateTime(2015, 3, 8, 3, 0, 0);
            Console.WriteLine("{0}, is {1} off of UTC, with respect to {2:o} of {3} kind.",
                tzName, 
                TimeZoneInfo.FindSystemTimeZoneById(tzName)
                    .GetUtcOffset(dtAfterPDT),
                dtBeforePDT, 
                dtBeforePDT.Kind);

            var dtBeforePDTLocal = new DateTime(2015, 3, 8, 1, 0, 0, DateTimeKind.Local);
            Console.WriteLine("{0}, is {1} off of UTC, with respect to {2:o} of {3} kind.",
                tzName, 
                TimeZoneInfo.FindSystemTimeZoneById(tzName)
                    .GetUtcOffset(dtBeforePDTLocal),
                dtBeforePDT, 
                dtBeforePDT.Kind);

            var dtAfterPDTLocal = new DateTime(2015, 3, 8, 3, 0, 0, DateTimeKind.Local);
            Console.WriteLine("{0}, is {1} off of UTC, with respect to {2:o} of {3} kind.",
                tzName, 
                TimeZoneInfo.FindSystemTimeZoneById(tzName)
                    .GetUtcOffset(dtAfterPDTLocal),
                dtBeforePDT, 
                dtBeforePDT.Kind);

            var dtoBeforePDT = new DateTimeOffset(2015, 3, 8, 1, 0, 0, new TimeSpan(-7, 0, 0));
            Console.WriteLine("{0}, is {2} off of UTC, with respect to {1:o}.", 
                tzName, 
                dtoBeforePDT,
                TimeZoneInfo.FindSystemTimeZoneById(tzName).GetUtcOffset(dtoBeforePDT));

            var dtoAfterPDT = new DateTimeOffset(2015, 3, 8, 3, 0, 0, new TimeSpan(-7, 0, 0));
            Console.WriteLine("{0}, is {2} off of UTC, with respect to {1:o}.", 
                tzName, 
                dtoAfterPDT,
                TimeZoneInfo.FindSystemTimeZoneById(tzName).GetUtcOffset(dtoAfterPDT));

        }
        private static bool EndsWithTimeZoneName(string response)
        {
            var tzs = TZNames.GetDisplayNames("en-US");

            return tzs.Any(tz => response.EndsWith(tz.Key, StringComparison.InvariantCultureIgnoreCase));
            //return new[] {"Eastern Standard Time", 
            //        "Central Standard Time", 
            //        "Pacific Standard Time"}
            //    .Any(tz => response.EndsWith(tz, StringComparison.CurrentCultureIgnoreCase));
        }

        [Test]
        public void ParseString()
        {
            var dtString = "2019-05-23T05:30:00 Eastern Standard Time";
            var x = InstantPattern.CreateWithInvariantCulture("yyyy-MM-dd'T'HH:mm:ss zzz").Parse(dtString);
            Assert.That(x, Is.Not.Null);
        }
        
        [TestCase("2019-05-23T05:30:00 Eastern Standard Time", -4)]
        [TestCase("2019-01-23T05:30:00 Eastern Standard Time", -5)]
        [TestCase("2019-05-23T05:30:00 Central Standard Time", -6)]
        [TestCase("2019-01-23T05:30:00 Central Standard Time", -7)]
        [TestCase("2019-05-23T05:30:00 Pacific Standard Time", -7)]
        [TestCase("2019-01-23T05:30:00 Pacific Standard Time", -8)]
        public void CanConvertToDateTimeOffset(string dtString, int offset)
        {
            if (EndsWithTimeZoneName(dtString))
            {
                var dtParts = dtString.Split(' ');

                var dt = DateTime.SpecifyKind(DateTime.Parse(dtParts[0]), DateTimeKind.Unspecified);
                var tz = TimeZoneInfo.FindSystemTimeZoneById(dtParts[1]);
               
                var datetimeOffset = new DateTimeOffset(dt, tz.GetUtcOffset(dt));
                Assert.That(datetimeOffset.Offset, Is.EqualTo(offset));
                Assert.That(datetimeOffset.Hour, Is.EqualTo(05));
            }
            else
            {
                Assert.Fail();
            }
        }

        [Test]
        public void AllNonDSTTimeZones()
        {
            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones().Where(z => !z.SupportsDaylightSavingTime).OrderBy(z => z.BaseUtcOffset))
            {
                var off = z.BaseUtcOffset;
                var name = z.StandardName;
                Console.WriteLine($"{name}: {off}");
            }
        }

        [Test]
        public void InvalidDate()
        {
            DateTimeZone brazEast = DateTimeZoneProviders.Tzdb["Brazil/East"];

            var dateExceptionForNegative1 = Assert.Throws<ArgumentException>(() =>
            {
                var invalidDateTime = new ZonedDateTime(new LocalDateTime(2018, 11, 4, 0, 0, 0), brazEast, Offset.FromHours(-1));
            });
            Assert.That(dateExceptionForNegative1.Message, Does.Contain("Offset -01 is invalid for local date and time 11/4/2018 12:00:00 AM in time zone Brazil/East"));

            var dateExceptionForNegative2 = Assert.Throws<ArgumentException>(() =>
            {
                var invalidDateTime = new ZonedDateTime(new LocalDateTime(2018, 11, 4, 0, 0, 0), brazEast, Offset.FromHours(-2));
            });
            Assert.That(dateExceptionForNegative2.Message, Does.Contain("Offset -02 is invalid for local date and time 11/4/2018 12:00:00 AM in time zone Brazil/East"));

            var dateExceptionForNegative3 = Assert.Throws<ArgumentException>(() =>
            {
                var invalidDateTime = new ZonedDateTime(new LocalDateTime(2018, 11, 4, 0, 0, 0), brazEast, Offset.FromHours(-3));
            });
            Assert.That(dateExceptionForNegative3.Message, Does.Contain("Offset -03 is invalid for local date and time 11/4/2018 12:00:00 AM in time zone Brazil/East"));

            var nodaInstant1 =
                Instant.FromDateTimeOffset(new DateTimeOffset(2018, 11, 4, 0, 0, 0, new TimeSpan(-1, 0, 0)));
            var invalidDateTime1 = new ZonedDateTime(nodaInstant1, brazEast);
            Console.WriteLine($"nodaDateTime: {invalidDateTime1:  MM-dd-yyyy HH:mm:ss}");

            var nodaInstant2 =
                Instant.FromDateTimeOffset(new DateTimeOffset(2018, 11, 4, 0, 0, 0, new TimeSpan(-2, 0, 0)));
            var invalidDateTime2 = new ZonedDateTime(nodaInstant2, brazEast);
            Console.WriteLine($"nodaDateTime: {invalidDateTime2:  MM-dd-yyyy HH:mm:ss}");

            var nodaInstant3 =
                Instant.FromDateTimeOffset(new DateTimeOffset(2018, 11, 4, 0, 0, 0, new TimeSpan(-3, 0, 0)));
            var invalidDateTime3 = new ZonedDateTime(nodaInstant3, brazEast);
            Console.WriteLine($"nodaDateTime: {invalidDateTime3:  MM-dd-yyyy HH:mm:ss}");

            var nodaInstant4 =
                Instant.FromDateTimeOffset(new DateTimeOffset(2018, 11, 4, 2, 0, 0, new TimeSpan(0, 0, 0)));
            var invalidDateTime4 = new ZonedDateTime(nodaInstant4, brazEast);
            Console.WriteLine($"nodaDateTime: {invalidDateTime4:  MM-dd-yyyy HH:mm:ss}");

            var nodaInstant5 =
                Instant.FromDateTimeOffset(new DateTimeOffset(2018, 11, 4, 3, 0, 0, new TimeSpan(0, 0, 0)));
            var invalidDateTime5 = new ZonedDateTime(nodaInstant5, brazEast);
            Console.WriteLine($"nodaDateTime: {invalidDateTime5:  MM-dd-yyyy HH:mm:ss}");

        }

        [Test]
        public void ValidDates()
        {
            DateTimeZone amerLa = DateTimeZoneProviders.Tzdb["Brazil/East"];

            ZonedDateTime dstDateTime = new ZonedDateTime(new LocalDateTime(2018, 11, 4, 1, 0, 0), amerLa, Offset.FromHours(-2));
            Console.WriteLine(dstDateTime.IsDaylightSavingTime());
            Console.WriteLine(dstDateTime.Zone.Id);
            Console.WriteLine(dstDateTime.LocalDateTime);
            Console.WriteLine(dstDateTime.ToDateTimeOffset());

            ZonedDateTime nonDstDateTime = new ZonedDateTime(new LocalDateTime(2018, 11, 3, 11, 59, 59), amerLa, Offset.FromHours(-3));
            Console.WriteLine(nonDstDateTime.IsDaylightSavingTime());
            Console.WriteLine(nonDstDateTime.Zone.Id);
            Console.WriteLine(nonDstDateTime.LocalDateTime);
            Console.WriteLine(nonDstDateTime.ToDateTimeOffset());
        }
    }
}
