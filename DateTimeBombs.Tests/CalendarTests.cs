﻿using System;
using System.Globalization;
using NodaTime;
using NUnit.Framework;

namespace DateTimeBombs.Tests
{
    [TestFixture]
    public class CalendarTests
    {
        [Test]
        public void CalendarDayOfWeek()
        {
            var myCi = new CultureInfo("en-US");
            //Calendar myCal = myCI.Calendar;
            Assert.That(myCi.DateTimeFormat.FirstDayOfWeek, Is.EqualTo(DayOfWeek.Sunday));
        }
        
        [Test]
        public void CalendarFirstWeek()
        {
            var myCi = new CultureInfo("en-US");
            Assert.That(myCi.DateTimeFormat.CalendarWeekRule, Is.EqualTo(CalendarWeekRule.FirstDay));
        }
        [Test]
        public void MonthsBetween()
        {
            LocalDate start = new LocalDate(2020, 1, 31);
            LocalDate end = new LocalDate(2020, 6, 29);
            Period period = Period.Between(start, end, PeriodUnits.AllDateUnits);
            Console.WriteLine($"Months: {period.Months}, Weeks: {period.Weeks}, Days: {period.Days}"); // 16
        }
    }
}