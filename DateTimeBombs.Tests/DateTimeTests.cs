﻿using System;
using System.Diagnostics;
using System.Linq;
using FastMember;
using NUnit.Framework;

namespace DateTimeBombs.Tests
{
    [TestFixture]
    public class DateTimeTests
    {
        private class TestClass
        {
            public DateTime MyDateTime;
        }

        [Test]
        public void TestBoxingAndUnboxing()
        {
            var runLengthTime = new TimeSpan(0, 1, 0);
            var timer = new Stopwatch();
            timer.Start();
            long numOfRuns = 0;
            do
            {
                numOfRuns++;
                var originDateTime = DateTime.Now;

                object dateObject = originDateTime;
                var unboxedDateTime = (DateTime) dateObject;
                Assert.That(originDateTime, Is.EqualTo(unboxedDateTime),
                    $"originDateTime: {originDateTime.Ticks} is not equal to unboxedDateTime: {unboxedDateTime.Ticks}.  After {numOfRuns} tries.");
            } while (timer.Elapsed < runLengthTime);
        }

        [Test]
        public void TestBoxing()
        {
            var runLengthTime = new TimeSpan(0, 1, 0);
            var timer = new Stopwatch();
            timer.Start();

            long numOfRuns = 0;
            do
            {
                numOfRuns++;
                var originDateTime = DateTime.Now;

                object dateObject1 = originDateTime;
                object dateObject2 = originDateTime;
                Assert.That(dateObject1, Is.EqualTo(dateObject2),
                    $"dateObject1: {((DateTime) dateObject1).Ticks} is not equal to dateObject2: {((DateTime) dateObject2).Ticks}.  After {numOfRuns} tries.");
            } while (timer.Elapsed < runLengthTime);

            Console.WriteLine($"Finished after {numOfRuns} tries.");
        }

        [Test]
        public void TestFastMember()
        {
            var runLengthTime = new TimeSpan(0, 0, 05);
            var timer = new Stopwatch();
            timer.Start();

            long numOfRuns = 0;
            do
            {

                numOfRuns++;
                var originDateTime = DateTime.Now;
                var testClass1 = new TestClass
                {
                    MyDateTime = originDateTime
                };
                var testClass2 = new TestClass
                {
                    MyDateTime = originDateTime
                };

                var accessor = TypeAccessor.Create(typeof(TestClass));

                foreach (var member in accessor.GetMembers())
                {
                    var testClass1Value = accessor[testClass1, member.Name];

                    var testClass2Value = accessor[testClass2, member.Name];

                    var actual = testClass1Value.Equals(testClass2Value);
                    Assert.IsTrue(actual,
                        $"testClass1Value: {((DateTime) testClass1Value).Ticks} is not equal to testClass2Value: {((DateTime) testClass2Value).Ticks}.  After {numOfRuns} tries.");
                }

            } while (timer.Elapsed < runLengthTime);

            Console.WriteLine($"Finished after {numOfRuns} tries.");

        }

        [Test]
        public void TestParseEqualities()
        {
            var runLengthTime = new TimeSpan(0, 1, 0);
            var timer = new Stopwatch();
            timer.Start();
            long numOfRuns = 0;
            do
            {
                numOfRuns++;
                var originDateTime = DateTime.Now;

                object dateObject = originDateTime;
                var unboxedDateTime = (DateTime) dateObject;
                Assert.That(originDateTime, Is.EqualTo(unboxedDateTime),
                    $"originDateTime: {originDateTime.Ticks} is not equal to unboxedDateTime: {unboxedDateTime.Ticks}.  After {numOfRuns} tries.");
            } while (timer.Elapsed < runLengthTime);
        }

        [Test]
        public void TestTimeAccuracy()
        {
            var runLengthTime = new TimeSpan(0, 0, 1);
            var timer = new Stopwatch();
            timer.Start();
            DateTime date = DateTime.Now;
            long ticks = date.Ticks;
            long sameCount = 0;
            Console.WriteLine($"Start time {date}");
            while (timer.Elapsed < runLengthTime)
            {
                if (ticks.Equals(DateTime.Now.Ticks))
                {
                    sameCount++;
                }
                else
                {
                    ticks = DateTime.Now.Ticks;
                    Console.WriteLine(ticks);
                    Console.WriteLine($"Time was the same {sameCount} times in a row.");
                    sameCount = 0;
                }
            }
            Console.WriteLine($"End time {DateTime.Now}");
        }
    

        [Test]
        public void DateTimeKindMattersInDateTimeOffsetConstructor()
        {
            var dateTimeExpected = DateTime.SpecifyKind(new DateTime(2016, 08, 01, 03, 04, 05), DateTimeKind.Utc);;
            var expected = new DateTimeOffset(dateTimeExpected, new TimeSpan(0, 0, 0));

            var dateTimeActualLocal = DateTime.SpecifyKind(new DateTime(2016, 08, 01, 03, 04, 05), DateTimeKind.Local);
            var actualLocal = new DateTimeOffset(dateTimeActualLocal);

            Assert.That(actualLocal, Is.Not.EqualTo(expected));

            var dateTimeActualUnspecified = DateTime.SpecifyKind(new DateTime(2016, 08, 01, 03, 04, 05), DateTimeKind.Unspecified);
            var actualUnspecified = new DateTimeOffset(dateTimeActualUnspecified, new TimeSpan(0, 0, 0));

            Assert.That(actualUnspecified, Is.EqualTo(expected));
        }


        [TestCase("2019-11-03T12:04:05")]
        [TestCase("2019-11-03T01:04:05")]
        [TestCase("2019-11-03T02:04:05")]
        public void ResolveAmbiguousTime(string ambiguousString)
        {
            var ambiguousTime = DateTime.SpecifyKind(DateTime.Parse(ambiguousString), DateTimeKind.Unspecified);
            // Time is not ambiguous
            if (! TimeZoneInfo.Local.IsAmbiguousTime(ambiguousTime))
            {
                Console.WriteLine("Time is not ambiguous");
                Assert.That(ambiguousTime, Is.EqualTo(ambiguousTime)); 
            }
            // Time is ambiguous
            else
            {
                TimeZoneInfo.Local.GetAmbiguousTimeOffsets(ambiguousTime).ToList().ForEach(t => Console.WriteLine(t));
                Console.WriteLine();
                DateTime utcTime = DateTime.SpecifyKind(ambiguousTime - TimeZoneInfo.Local.BaseUtcOffset, 
                    DateTimeKind.Utc);      
                Console.WriteLine("{0} local time corresponds to {1} {2}.", 
                    ambiguousTime, utcTime, utcTime.Kind.ToString());
                Assert.That(utcTime, Is.EqualTo(ambiguousTime.ToUniversalTime()));        
            }
        }
        
        [Test]
        public void IsInvalidDate_For_DateTime()
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            var dateTime = DateTime.SpecifyKind(new DateTime(2019, 03, 10, 2, 30, 0), DateTimeKind.Unspecified);
           
            Assert.That(tz.IsInvalidTime(dateTime), Is.True);
        }

        [Test]
        public void IsNotInvalidBeforeSpringForwardDate_For_DateTime()
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            var dateTime = DateTime.SpecifyKind(new DateTime(2019, 03, 10, 1, 30, 0), DateTimeKind.Unspecified);
            
            Assert.That(tz.IsInvalidTime(dateTime), Is.False);
        }

        [Test]
        public void IsNotInvalid_But_IsAmbiguousBeforeFallBackForwardDate_For_DateTime()
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            var dateTime = DateTime.SpecifyKind(new DateTime(2019, 11, 03, 1, 30, 0), DateTimeKind.Unspecified);
            
            Assert.That(tz.IsInvalidTime(dateTime), Is.False);
            Assert.That(tz.IsAmbiguousTime(dateTime), Is.True);
        }

        [Test]
        public void IsNotInvalidAfterSpringForwardDate_For_DateTime()
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            var dateTime = DateTime.SpecifyKind(new DateTime(2019, 03, 10, 3, 00, 0), DateTimeKind.Unspecified);
            
            Assert.That(tz.IsInvalidTime(dateTime), Is.False);
        }

        [Test]
        public void IsInvalidDate_For_DateTimeOffset()
        {
            var tzId = "Pacific Standard Time";
            var tz = TimeZoneInfo.FindSystemTimeZoneById(tzId);

            foreach (var adjustmentRule in tz.GetAdjustmentRules())
            {
                Console.WriteLine($"DateStart: {adjustmentRule.DateStart}");
                Console.WriteLine($"DateEnd: {adjustmentRule.DateEnd}");
                Console.WriteLine($"DaylightDelta: {adjustmentRule.DaylightDelta}");

                Console.WriteLine($"DaylightTransitionStart: {adjustmentRule.DaylightTransitionStart.DayOfWeek}, {adjustmentRule.DaylightTransitionStart.Month}-{adjustmentRule.DaylightTransitionStart.Day} {adjustmentRule.DaylightTransitionStart.TimeOfDay.ToShortTimeString()}");
                Console.WriteLine($"DaylightTransitionEnd: {adjustmentRule.DaylightTransitionEnd.DayOfWeek}, {adjustmentRule.DaylightTransitionEnd.Month}-{adjustmentRule.DaylightTransitionEnd.Day} {adjustmentRule.DaylightTransitionEnd.TimeOfDay.ToShortTimeString()}");
            }

            var dateTimeOffset = new DateTimeOffset(2019, 03, 10, 2, 30, 0, new TimeSpan(-7, 0, 0));
            var x = tz.IsInvalidTime(dateTimeOffset.DateTime);
            Assert.That(x, Is.True);
        }


        [TestCase("2019-11-03T12:04:05.000-07:00", "Mountain Standard Time")]
        [TestCase("2019-11-03T01:04:05.000-07:00", "Mountain Standard Time")]
        [TestCase("2019-11-03T01:04:05.000-06:00", "Mountain Standard Time")]
        [TestCase("2019-11-03T02:04:05.000-07:00", "Mountain Standard Time")]
        [TestCase("2019-11-03T03:04:05.000-07:00", "Pacific Standard Time")]
        //Fails
        [TestCase("2019-11-03T12:04:05.000-07:00", "Pacific Standard Time")]
        //[TestCase("2019-11-03T01:04:05.000-07:00", "Pacific Standard Time")]
        public void ResolveAmbiguousTimeOffset(string ambiguousString, string tzName)
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById(tzName);
            var ambiguousTime = DateTimeOffset.Parse(ambiguousString);
            // Time is not ambiguous
            if (! tz.IsAmbiguousTime(ambiguousTime))
            {
                Console.WriteLine("Time is not ambiguous");
                Assert.That(ambiguousTime, Is.EqualTo(ambiguousTime)); 
            }
            // Time is ambiguous
            else
            {
                tz.GetAmbiguousTimeOffsets(ambiguousTime).ToList().ForEach(t => Console.WriteLine(t));
                Console.WriteLine();
                DateTime utcTime = DateTime.SpecifyKind(ambiguousTime.DateTime - tz.BaseUtcOffset, 
                    DateTimeKind.Utc);      
                Console.WriteLine("{0} local time corresponds to {1} {2}.", 
                    ambiguousTime, utcTime, utcTime.Kind.ToString());
                Assert.That(utcTime, Is.EqualTo(ambiguousTime.ToUniversalTime().DateTime));        
            }
        }

        [Test]
        public void DateTimeComparisonsIgnoreKind()
        {
            var datetime = new DateTime(2019, 07, 04, 12, 00, 00);
            DateTime localTime = DateTime.SpecifyKind(datetime, DateTimeKind.Local);
            DateTime utcTime = localTime.ToUniversalTime();

            Assert.That(localTime.CompareTo(utcTime), Is.EqualTo(-1));
            Assert.That(localTime, Is.LessThan(utcTime));
            Assert.That(localTime < utcTime, Is.True);
        }

        [Test]
        public void CompareSameDateTimeOffsets()
        {
            DateTimeOffset localTime = new DateTimeOffset(2019, 07, 04, 12, 00, 00, new TimeSpan(-7, 0, 0));
            DateTimeOffset utcTime = new DateTimeOffset(2019, 07, 04, 19, 00, 00, new TimeSpan(0, 0, 0));

            Assert.That(localTime.CompareTo(utcTime), Is.EqualTo(0));
            Assert.That(localTime, Is.EqualTo(utcTime));
            Assert.That(localTime == utcTime, Is.True);
        }

        [Test]
        public void CompareSameConvertedDates()
        {
            // This can happen because
            DateTime localTime = DateTime.Now;
            DateTime utcTime = DateTime.UtcNow;
            var convertedToUtc = localTime.ToUniversalTime();

            Console.WriteLine($"convertedToUtc: {convertedToUtc:O}, utcTime: {utcTime:O}");
            Assert.That(convertedToUtc, Is.LessThan(utcTime));
            Assert.That(convertedToUtc < utcTime, Is.True);
        }
    }
}