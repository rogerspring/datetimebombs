﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NodaTime;
using NodaTime.Serialization.JsonNet;
using NodaTime.Testing;
using NodaTime.Text;
using NUnit.Framework;

namespace DateTimeBombs.Tests
{
    public class MyDateTimeShell
    {
        public Instant Instant { get; set; }
        public OffsetDateTime OffsetDateTime { get; set; }
        public ZonedDateTime ZonedDateTime { get; set; }
        public LocalDateTime LocalDateTime { get; set; }
        public LocalDate LocalDate { get; set; }
        public LocalTime LocalTime { get; set; }
        public Offset Offset { get; set; }
        public Interval Interval { get; set; }
        public Duration Duration { get; set; }
        public Period Period { get; set; }
    }

    [TestFixture]
    public class NodaTimeJsonSerialization
    {
        private IClock GetTimeProvider()
        {
            return FakeClock.FromUtc(2019, 01, 01);
            //var today = new ZonedClock(fakeClock, DateTimeZone.Utc, CalendarSystem.Gregorian).GetCurrentDate();
        }

        private static IDateTimeZoneProvider GetDateTimeZoneProvider()
        {
            var dateTimeZoneProvider = DateTimeZoneProviders.Tzdb;
            return dateTimeZoneProvider;
        }


        [Test]
        public void TryParsingZonedDateTime()
        {
            var fakeClock = GetTimeProvider();

            var time = "4:30pm";
            var timezone = "America/Los_Angeles";

            // parse the time string using Noda Time's pattern API
            var pattern = LocalTimePattern.CreateWithCurrentCulture("h:mmtt");
            ParseResult<LocalTime> parseResult = pattern.Parse(time);
            if (!parseResult.Success)
            {
                // handle parse failure
            }

            var localTime = parseResult.Value;

            // get the current date in the target time zone
            var dateTimeZone = DateTimeZoneProviders.Tzdb.GetZoneOrNull(timezone);

            if (dateTimeZone is null) throw new Exception("time zone not found");
            var instant = fakeClock.GetCurrentInstant();
            var localDate = fakeClock.GetCurrentInstant().InZone(dateTimeZone).Date;


            // combine the date and time
            var localDateTime = localDate.At(localTime);


            // bind it to the time zone
            var zonedDateTime = localDateTime.InZoneLeniently(dateTimeZone);

            var offset = zonedDateTime.Offset;

            var duration = Duration.FromDays(590);

            var interval = new Interval(fakeClock.GetCurrentInstant(), fakeClock.GetCurrentInstant().Plus(duration));

            var pb = new PeriodBuilder
            {
                Years = 1,
                Months = 2,
                Weeks = 33,
                Days = 15,
                Hours = 28,
                Minutes = 71,
                Seconds = 93
            };
            var localDateTimeEnd = localDateTime.Minus(pb.Build());
            var period = Period.Between(localDateTime, localDateTimeEnd);

            var sut = new MyDateTimeShell
            {
                Instant = instant,
                OffsetDateTime = zonedDateTime.ToOffsetDateTime(),
                ZonedDateTime = zonedDateTime,
                LocalDateTime = localDateTime,
                LocalDate = localDate,
                LocalTime = localTime,
                Offset = offset,
                Interval = interval,
                Duration = duration,
                Period = period.Normalize()
            };

            var settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            settings.ConfigureForNodaTime(GetDateTimeZoneProvider());
            var actual = JsonConvert.SerializeObject(sut, settings);

            Console.WriteLine(actual);

            Assert.That(actual, Is.Not.Null);
        }
    }
}