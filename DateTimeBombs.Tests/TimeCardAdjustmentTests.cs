﻿using System;
using NodaTime;
using NUnit.Framework;

namespace DateTimeBombs.Tests
{
    [TestFixture]
    public class TimeCardAdjustmentTests
    {

        /*
         * Knowing the offset, but not the timezone we can only assume if and when
         * daylight savings was in effect and whether adjustments to said time should
         * be affected by DST.
         */

        private class TimeCard
        {
            public DateTimeOffset ClockedIn;
            public DateTimeOffset ClockedOut;

            public TimeSpan TotalTimeWorked()
            {
                return ClockedOut.Subtract(ClockedIn);
            }
        }

        private class ZonedTimeCard
        {
            public ZonedDateTime ClockedIn;
            public ZonedDateTime ClockedOut;

            public Duration TotalTimeWorked()
            {
                return ClockedOut.Minus(ClockedIn);
            }
        }


        /// <summary>
        /// Scenario 1.  Bob the bartender was at work from 6:41 to 1:41am.
        /// But he clocked out prematurely at 12:41am by mistake.  Actually left two hours after that.
        /// Let's add two hours to his clock-out time
        /// </summary>
        [Test]
        public void TestTimeAdjustmentByAddingHour()
        {
            DateTimeOffset clockedIn = new DateTimeOffset(2018, 11, 03, 18, 21, 32, new TimeSpan(-7, 0, 0));
            // {11/3/2018 6:21:32 PM -07:00}

            DateTimeOffset clockedOut = new DateTimeOffset(2018, 11, 04, 00, 48, 07, new TimeSpan(-7, 0, 0));
            // {11/4/2018 12:48:07 AM -07:00}

            TimeCard timeCard = new TimeCard
            {
                ClockedIn = clockedIn,
                ClockedOut = clockedOut
            };

            TimeSpan before = timeCard.TotalTimeWorked();
            // Bob's time card is only showing {06:26:35} hour so far

            // In America/Los Angeles the time looks like this...
            //----12----12:30----1----1:30----1----1:30----2----2:30----3----//

            // In America/Phoenix the time looks like this...
            //----12----12:30----1----1:30----2----2:30----3----3:30----4----//

            // Let's add an hour to the employee's time card.
            timeCard.ClockedOut = clockedOut.AddHours(2);
            // {11/4/2018 2:48:07 AM -07:00}
            // In America/Phoenix this is what just happened...
            //----12----12:30----1----1:30----2----2:30----3----3:30----4----//
            //-----------------X>>>>>>>>>>>>>>>>>>>>>>>>>X
            TimeSpan afterAssumedTimeZone = timeCard.TotalTimeWorked();
            // {08:26:35}

            // timeCard.ClockedOut.ToLocalTime().ToString("MM-dd-yyyy HH:mm:ss tt")
            // "11-04-2018 01:48:07 AM"
            // But this leaves is looking like Bob was in AZ.
            // For America/Los Angeles time zone, this is an invalid offset for this date and time.
            // So, if we took this time and displayed it in a UI, the time would show an hour off, causing confusion.
            Console.WriteLine($"Clocked out: {timeCard.ClockedOut: MM-dd-yyyy HH:mm:ss tt zzz}");

            //Note the 'timezone' id here is actually worded as an offset id.
            TimeZoneInfo pacificZone = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");

            var pacificTime = TimeZoneInfo.ConvertTime(timeCard.ClockedOut, pacificZone);

            Console.WriteLine($"As Pacific time: {pacificTime: MM-dd-yyyy HH:mm:ss}");
            Console.WriteLine($"As Local: {timeCard.ClockedOut.ToLocalTime(): MM-dd-yyyy HH:mm:ss}");

            var nodaClockedOut = Instant.FromDateTimeOffset(timeCard.ClockedOut);

            var zone = DateTimeZoneProviders.Tzdb["America/Los_Angeles"];
            var nodaDateTime = new ZonedDateTime(nodaClockedOut, zone);
            Console.WriteLine($"nodaDateTime: {nodaDateTime:  MM-dd-yyyy HH:mm:ss}");


            // In America/Los Angeles this is what should have happened...
            //----12----12:30----1----1:30----1----1:30----2----2:30----3----//
            //-----------------X>>>>>>>>>>>>>>>>>>>>>>>>>X
            // Note, just before 2am, the offset changes to -08:00.

            // So, should this have been set to {11/4/2018 1:48:07 AM -08:00}?
            // If so, this would be necessary to add an hour by 1.
            var correctedTime = clockedOut.AddHours(2).ToOffset(new TimeSpan(-8, 0, 0));
            timeCard.ClockedOut = correctedTime;
            // {11/4/2018 1:48:07 AM -08:00}

            // The total time is the same either way.  So, it won't negatively affect payroll for this person.
            TimeSpan afterCorrectOffset = timeCard.TotalTimeWorked();
            // {08:26:35}

            // Everything still gets rendered the same as a local string.
            // timeCard.ClockedOut.ToLocalTime().ToString("MM-dd-yyyy HH:mm:ss tt")
            // "11-04-2018 01:48:07 AM"



            Assert.That(afterCorrectOffset.Days, Is.Zero);
            Assert.That(afterCorrectOffset.Hours, Is.EqualTo(8));
            Assert.That(afterCorrectOffset.Minutes, Is.EqualTo(26));
            Assert.That(afterCorrectOffset.Seconds, Is.EqualTo(35));
        }

        [Test]
        public void TestTimeAdjustmentBySettingDate()
        {
            DateTimeOffset clockedIn = new DateTimeOffset(2018, 11, 03, 18, 21, 32, new TimeSpan(-7, 0, 0));
            // {11/3/2018 6:21:32 PM -07:00}

            DateTimeOffset clockedOut = new DateTimeOffset(2018, 11, 04, 00, 48, 07, new TimeSpan(-7, 0, 0));
            // {11/4/2018 12:48:07 AM -07:00}
            
            TimeCard timeCard = new TimeCard
            {
                ClockedIn = clockedIn,
                ClockedOut = clockedOut
            };

            TimeSpan before = timeCard.TotalTimeWorked();
            // {06:26:35}

            /*
             * If we set the date directly, what offset do we use?
             */
            timeCard.ClockedOut = new DateTimeOffset(2018, 11, 04, 01, 48, 07, new TimeSpan(-7, 0, 0));
            // {11/4/2018 1:48:07 AM -07:00}

            TimeSpan assumedInAZ = timeCard.TotalTimeWorked();
            // {07:26:35}

            // or is it ?
            timeCard.ClockedOut = new DateTimeOffset(2018, 11, 04, 01, 48, 07, new TimeSpan(-8, 0, 0));
            // {11/4/2018 1:48:07 AM -08:00}
            TimeSpan assumedInOR = timeCard.TotalTimeWorked();
            // {08:26:35}

            Assert.That(assumedInOR.Days, Is.Zero);
            Assert.That(assumedInOR.Hours, Is.EqualTo(8));
            Assert.That(assumedInOR.Minutes, Is.EqualTo(26));
            Assert.That(assumedInOR.Seconds, Is.EqualTo(35));
        }

        [Test]
        public void TestTimeAdjustmentWithNodaTime()
        {
            DateTimeZone amerLA = DateTimeZoneProviders.Tzdb["America/Los_Angeles"];
            var start = new LocalDateTime(2018, 11, 03, 18, 21, 32);
            // {11/3/2018 6:41:32 PM}

            ZonedDateTime startTime = new ZonedDateTime(start, amerLA, Offset.FromHours(-7));
            // {2018-11-03T18:21:32 America/Los_Angeles (-07)}

            var end = new LocalDateTime(2018, 11, 04, 00, 48, 07);
            // {11/4/2018 12:48:07 AM}

            ZonedDateTime endTime = new ZonedDateTime(end, amerLA, Offset.FromHours(-7));
            // {2018-11-04T00:48:07 America/Los_Angeles (-07)}

            ZonedTimeCard timeCard = new ZonedTimeCard
            {
                ClockedIn = startTime,
                ClockedOut = endTime
            };

            Duration before = timeCard.TotalTimeWorked();
            // {06:26:35}

            var adjust1 = new ZonedDateTime(new LocalDateTime(2018, 11, 04, 00, 48, 07), amerLA, Offset.FromHours(-7));
            // Can also be expressed as ZonedDateTime.Subtract(...);
            timeCard.ClockedOut = ZonedDateTime.Add(adjust1, Duration.FromHours(2));
            // {2018-11-04T01:48:07 America/Los_Angeles (-08)}

            Duration after1 = timeCard.TotalTimeWorked();
            // {0:08:26:35}

            var adjust2 = new ZonedDateTime(new LocalDateTime(2018, 11, 04, 00, 48, 07), amerLA, Offset.FromHours(-7));
            timeCard.ClockedOut = ZonedDateTime.Add(adjust2, Duration.FromHours(1));
            // {2018-11-04T01:41:32 America/Los_Angeles (-07)}

            Duration after = timeCard.TotalTimeWorked();
            // {0:07:26:35}

            Assert.That(after.Days, Is.Zero);
            Assert.That(after.Hours, Is.EqualTo(7));
            Assert.That(after.Minutes, Is.EqualTo(26));
            Assert.That(after.Seconds, Is.EqualTo(35));
        }
    }
}