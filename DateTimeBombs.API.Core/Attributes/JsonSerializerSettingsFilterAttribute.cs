using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Buffers;
using System.Collections.Generic;
using System.Text.Json;

namespace DateTimeBombs.API.Core.Attributes
{
    public class JsonSerializerSettingsFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Result is ObjectResult objectResult)
            {
                objectResult.Formatters.Add(new NewtonsoftJsonOutputFormatter(
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                        DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                        Converters = new List<JsonConverter> { new StringEnumConverter() },
                    },
                    context.HttpContext.RequestServices.GetRequiredService<ArrayPool<char>>(),
                    context.HttpContext.RequestServices.GetRequiredService<IOptions<MvcOptions>>().Value));

                JsonSerializerOptions serializerOptions = new JsonSerializerOptions();
                serializerOptions.Converters.Add(new DateTimeConverter());

                objectResult.Formatters.Add(new SystemTextJsonOutputFormatter(serializerOptions));
            }
        }
    }








}
