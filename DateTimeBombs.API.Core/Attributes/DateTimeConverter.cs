using System;
using System.Diagnostics;
using System.Globalization;
using System.Text.Json;

namespace DateTimeBombs.API.Core.Attributes
{
    public class DateTimeConverter : System.Text.Json.Serialization.JsonConverter<DateTime>
    {
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            DateTime result;
            Debug.Assert(typeToConvert == typeof(DateTime));
            string value = reader.GetString();
            DateTime parsedDateTime;
            
            var formatStrings = new string[] { 
                // ISO 8601 formats
                "yyyy-MM-ddThh:mm:ss", "yyyy-MM-ddThh:mm:ssZ",
                // Non-ISO 8601 formats
                "yyyy-MM-dd hh:mm:ss", "MM/dd/yyyy hh:mm:ss", "MM-dd-yyyy hh:mm:ss"
            };
            var p = DateTime.Parse(value, null, DateTimeStyles.AdjustToUniversal);
            Console.WriteLine($"Parse made {p:O}");

            if (DateTime.TryParseExact(value, formatStrings, new CultureInfo("en-US"), DateTimeStyles.AdjustToUniversal, out parsedDateTime))
            {
                result = parsedDateTime.Kind switch
                {
                    DateTimeKind.Utc => parsedDateTime,
                    DateTimeKind.Local => DateTime.SpecifyKind(DateTime.Parse(reader.GetString()), DateTimeKind.Utc),
                    _ => DateTime.SpecifyKind(DateTime.Parse(reader.GetString()), DateTimeKind.Utc),
                };
            } else
            {
                result = DateTime.MinValue;
            }
              
            return result;
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            if (value.Kind == DateTimeKind.Unspecified)
            {
                writer.WriteStringValue(value.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssZ"));
            } else
            {
                writer.WriteStringValue(value.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssZ"));
            }
        }
    }








}
