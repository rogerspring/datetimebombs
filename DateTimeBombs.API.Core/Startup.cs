using System.Collections.Generic;
using DateTimeBombs.DB;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using NodaTime;
using NodaTime.Serialization.JsonNet;

namespace DateTimeBombs.API.Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //.AddNewtonsoftJson(options =>
            //{
            //    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //    //options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            //    options.SerializerSettings.Converters = new List<JsonConverter> {new StringEnumConverter()};
            //    options.SerializerSettings.ConfigureForNodaTime(NodaTime.DateTimeZoneProviders.Tzdb);
            //});
            
            //.AddJsonOptions(options =>
                //{
                //    options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
                //})
                

            /**** OR *****/

            //.AddNewtonsoftJson(options =>
            //{
            //    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            //    options.SerializerSettings.Converters = new List<JsonConverter> { new StringEnumConverter() };
            //    options.SerializerSettings.ConfigureForNodaTime(NodaTime.DateTimeZoneProviders.Tzdb);
            //});

            services.AddSingleton<IClock, AppDateTimeProvider>();

            services.AddDbContext<DateTimeDbContext>(
                optionsAction: options =>
                {
                    options.UseSqlServer(
                        connectionString: Configuration.GetConnectionString("DefaultConnection")
                        );
                },
                contextLifetime: ServiceLifetime.Scoped,
                optionsLifetime: ServiceLifetime.Singleton);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
