﻿using NodaTime;

namespace DateTimeBombs.API.Core
{
    public class AppDateTimeProvider : IClock
    {
        public Instant GetCurrentInstant()
        {
            return SystemClock.Instance.GetCurrentInstant();
        }
    }
}
