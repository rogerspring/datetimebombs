﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DateTimeBombs.DB;
using DateTimeBombs.DB.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NodaTime;

namespace DateTimeBombs.API.Core.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GroceryController : ControllerBase
    {
        private readonly ILogger<GroceryController> _logger;
        private readonly IConfiguration _configuration;

        public GroceryController(ILogger<GroceryController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet("SeedMeSeymour")]
        public IActionResult Seed() => Ok(new Seeder(_configuration).SeedMeSeymour());

        [HttpGet("GetNonExpired")]
        public IEnumerable<GroceryItem> GetNonExpired() => new QueryExamples(_configuration).GetNonExpired();

        [HttpGet("GetByCreationInstant")]
        public IEnumerable<GroceryItem> GetByCreationDate() => new QueryExamples(_configuration).GetByCreationInstant();

        [HttpGet("GetByCreationInstantIsNull")]
        public IEnumerable<GroceryItem> GetByCreationInstantIsNull() => new QueryExamples(_configuration).GetByCreationInstantIsNull();
        
        [HttpGet("GetBySomeOtherFutureDate")]
        public IEnumerable<GroceryItem> GetBySomeOtherFutureDate() => new QueryExamples(_configuration).GetBySomeOtherFutureDate();

        [HttpGet("GetBySomeOtherPastDate")]
        public IEnumerable<GroceryItem> GetBySomeOtherPastDate() => new QueryExamples(_configuration).GetBySomeOtherPastDate();
    }
}
