﻿using System;
using DateTimeBombs.API.Core.Attributes;
using Microsoft.AspNetCore.Mvc;
using NodaTime;

namespace DateTimeBombs.API.Core.Controllers
{	
    [Route("CustomSerializer")]
    public class CustomSerializerController : ApiBaseController
    {
        public CustomSerializerController(IClock appClock) : base(appClock)
        {
        }

        [Route("default-current-time")]
        [HttpGet]
        public DateTime DefaultCurrentTime()
        {
            return DateTime.Now;
        }

        [JsonSerializerSettingsFilter]
        [Route("current-time")]
        [HttpGet]
        public DateTime CurrentTime()
        {
            return DateTime.Now;
        }

        [Route("")]
        [HttpGet]
        public DateTime Headless(DateTime date)
        {
            return date;
        }

        [Route("default-serialize")]
        [HttpPost]
        public DateTime DefaultSerialize([FromBody] TestDate date)
        {
            return date.date;
        }

        [JsonSerializerSettingsFilter]
        [Route("serialize")]
        [HttpPost]
        public DateTime Serialize([FromBody] TestDate date)
        {
            var k = date.date.Kind;
            date.date = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
            return date.date;
        }

        [Route("as-utc")]
        [HttpGet]
        public object asUTC([FromQuery] DateTimeOffset dateTime)
        {
            return new
            {
                enteredAsLocal = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, DateTimeKind.Local),
                enteredAsLocalDate = LocalDateTime.FromDateTime(dateTime.DateTime),
                enteredAsOffset = dateTime,
                utcAsOffset = dateTime.ToUniversalTime()
            };
        }

        public class TestDate
        {
            public DateTime date { get; set; }
        }
    }
}
