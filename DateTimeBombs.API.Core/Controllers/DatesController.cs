﻿using System;
using Microsoft.AspNetCore.Mvc;
using NodaTime;

namespace DateTimeBombs.API.Core.Controllers
{	
    [Route("dates")]
    public class DatesController : ApiBaseController
    {
        public DatesController(IClock appClock) : base(appClock)
        {
        }

        [Route("current-time")]
        [HttpGet]
        public DateTime CurrentTime()
        {
            return DateTime.Now;
        }

        [Route("")]
        [HttpGet]
        public DateTime Headless(DateTime date)
        {
            return date;
        }

        [Route("Serialize")]
        [HttpPost]
        public DateTime Serialize(DateTime date)
        {
            return date;
        }

        [Route("as-utc")]
        [HttpGet]
        public object asUTC([FromQuery] DateTimeOffset dateTime)
        {
            return new
            {
                enteredAsLocal = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, DateTimeKind.Local),
                enteredAsLocalDate = LocalDateTime.FromDateTime(dateTime.DateTime),
                enteredAsOffset = dateTime,
                utcAsOffset = dateTime.ToUniversalTime()
            };
        }
    }
}
