﻿using System;
using DateTimeBombs.API.Core.Controllers;
using Microsoft.AspNetCore.Mvc;
using NodaTime;
using NodaTime.Extensions;

namespace DateTimeBombs.API.Controllers
{	
    [Route("nodatypes")]
    public class NodaTypesController : ApiBaseController
    {
        public NodaTypesController(IClock appClock) : base(appClock)
        {
        }

        [HttpGet, Route("")]
        public DateTimeOffset Get()
        {
            return AppClock.GetCurrentInstant().ToDateTimeOffset();
        }

        // Does not work - Unsupported media type
        [HttpGet, Route("EchoLocalDateParam")]
        public string GetEchoLocalDateParam(LocalDate localDate)
        {
            return localDate.ToString();
        }

        // Does not work - Unsupported media type
        [HttpGet, Route("EchoLocalDate/{localDate}")]
        public string GetEchoLocalDate(LocalDate localDate)
        {
            return localDate.ToString();
        }

        [HttpGet, Route("LocalDate")]
        public LocalDate GetLocalDate()
        {
            var x = new LocalDate(2019, 01, 01);
            return x;
        }

        [HttpGet, Route("TodayAsLocalDate")]
        public LocalDate GetTodayAsLocalDate()
        {
            var t = DateTime.UtcNow;
            var x = new LocalDate(t.Year, t.Month, t.Day);
            return x;
        }

        [HttpGet, Route("DayOfWeek")]
        public IsoDayOfWeek GetDayOfWeek()
        {
            return AppClock.GetCurrentInstant().ToDateTimeOffset().DayOfWeek.ToIsoDayOfWeek();
        }

        [HttpGet, Route("ZonedDateTime")]
        public ZonedDateTime GetZonedDateTime()
        {
            DateTimeZone zone = DateTimeZoneProviders.Tzdb["America/Los_Angeles"];
            return AppClock.GetCurrentInstant().InZone(zone);
        }

        [HttpGet, Route("Duration")]
        public Duration GetDuration()
        {
            return Duration.FromDays(1);
        }



        [HttpGet, Route("AllNodaTypes")]
        public NodaTypesSampler GetAllNodaTypes()
        {
            DateTimeZone zone = DateTimeZoneProviders.Tzdb["America/Los_Angeles"];
            ZonedDateTime zonedDateTime = AppClock.GetCurrentInstant().InZone(zone);
            Duration duration = Duration.FromDays(1);
            DateInterval dateInterval = new DateInterval(zonedDateTime.Date, zonedDateTime.Plus(duration).Date);
            Instant instantEnd = zonedDateTime.Plus(Duration.FromHours(4)).ToInstant();
            Interval interval = new Interval(AppClock.GetCurrentInstant(), instantEnd);
            PeriodBuilder pb = new PeriodBuilder {Months = 3};
            Period period = pb.Build();

            var x = new NodaTypesSampler
            {
                DateInterval = dateInterval,
                DateTimeZone = zone,
                Duration = duration,
                Instant = AppClock.GetCurrentInstant(),
                Interval = interval,
                IsoDayOfWeek = IsoDayOfWeek.Thursday,
                LocalDate = zonedDateTime.Date,
                LocalDateTime = zonedDateTime.LocalDateTime,
                LocalTime = zonedDateTime.TimeOfDay,
                Offset = zone.GetUtcOffset(AppClock.GetCurrentInstant()),
                OffsetDate = zonedDateTime.ToOffsetDateTime().ToOffsetDate(),
                OffsetDateTime = zonedDateTime.ToOffsetDateTime(),
                OffsetTime = zonedDateTime.ToOffsetDateTime().ToOffsetTime(),
                Period = period,
                ZonedDateTime = zonedDateTime
            };
            
            return x;
        }
    }

    public class NodaTypesSampler
    {
        public DateInterval DateInterval { get; set; }
        public DateTimeZone DateTimeZone { get; set; }
        public Duration Duration { get; set; }
        public Instant Instant { get; set; }
        public Interval Interval { get; set; }
        public IsoDayOfWeek IsoDayOfWeek { get; set; }
        public LocalDate LocalDate { get; set; }
        public LocalDateTime LocalDateTime { get; set; }
        public LocalTime LocalTime { get; set; }
        public Offset Offset { get; set; }
        public OffsetDate OffsetDate { get; set; }
        public OffsetDateTime OffsetDateTime { get; set; }
        public OffsetTime OffsetTime { get; set; }
        public Period Period { get; set; }
        public ZonedDateTime ZonedDateTime { get; set; }
    }
}
