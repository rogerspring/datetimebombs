﻿using System.Web.Http;
using NodaTime;

namespace DateTimeBombs.API.Core.Controllers
{
    public abstract class ApiBaseController : ApiController
    {
        protected IClock AppClock;

        protected ApiBaseController(IClock appClock)
        {
            AppClock = appClock;
        }
    }
}
