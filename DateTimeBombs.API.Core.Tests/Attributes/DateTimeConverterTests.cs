﻿using DateTimeBombs.API.Core.Attributes;
using FluentAssertions;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using Xunit;

namespace DateTimeBombs.API.Core.Tests.Attributes
{
    public class DateTimeConverterTests
    {
        private DateTimeConverter CreateDateTimeConverter()
        {
            return new DateTimeConverter();
        }

        [Theory]
        [InlineData(@"""2001-02-03T00:00:00Z""")]
        [InlineData(@"""2001-02-03T00:00:00""")]
        [InlineData(@"""2001-02-03 00:00:00""")]
        [InlineData(@"""02-03-2001 00:00:00""")]
        [InlineData(@"""02/03/2001 00:00:00""")]
        public void Read_StateUnderTest_ExpectedBehavior(string input)
        {
            // Arrange
            var dateTimeConverter = CreateDateTimeConverter();

            var utf8JsonReader = new Utf8JsonReader(Encoding.UTF8.GetBytes(input),
                false, new JsonReaderState(new JsonReaderOptions()));

            utf8JsonReader.TokenType.Should().Be(JsonTokenType.None);
            utf8JsonReader.Read();
            var result = dateTimeConverter.Read(ref utf8JsonReader,
                typeof(DateTime), new JsonSerializerOptions { IgnoreNullValues = true });

            result.Should().Be(new DateTime(2001, 02, 03, 00, 00, 00, 0, DateTimeKind.Utc));
        }

        public static readonly object[][] DateData =
        {
            new object[] { new DateTime(2001, 02, 03, 00, 00, 00, 0, DateTimeKind.Utc)},
            new object[] { new DateTime(2001, 02, 02, 16, 00, 00, 0, DateTimeKind.Local)},
            new object[] { new DateTime(2001, 02, 03, 00, 00, 00, 0, DateTimeKind.Unspecified)},
        };

        [Theory, MemberData(nameof(DateData))]
        public void Write_StateUnderTest_ExpectedBehavior(DateTime value)
        {
            var dateTimeConverter = CreateDateTimeConverter();
            using (var test_Stream = new MemoryStream())
            {
                Utf8JsonWriter writer = new Utf8JsonWriter(test_Stream);

                JsonSerializerOptions options = null;

                dateTimeConverter.Write(
                    writer,
                    value,
                    options);
                writer.Flush();
                string result = Encoding.UTF8.GetString(test_Stream.ToArray());
                result.Should().Be(@"""2001-02-03T00:00:00Z""");
            }

        }


    }
}

