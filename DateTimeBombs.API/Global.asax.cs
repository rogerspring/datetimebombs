﻿using System.Web.Http;

namespace DateTimeBombs.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            UnityConfig.RegisterComponents(); 
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //'http://localhost:8080'
        }
    }
}
