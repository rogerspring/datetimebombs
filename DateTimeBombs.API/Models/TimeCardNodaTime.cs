﻿using NodaTime;

namespace DateTimeBombs.API.Models
{
    public class TimeCardNodaTime
    {
 
        public LocalDate PostingDate { get; set; }
        public ZonedDateTime ClockIn { get; set; }
        public ZonedDateTime ClockOut { get; set; }

        public Duration Duration => ClockOut.ToInstant() - ClockIn.ToInstant();

        public Interval TimeWorked => new Interval(ClockIn.ToInstant(), ClockOut.ToInstant());
    }
}