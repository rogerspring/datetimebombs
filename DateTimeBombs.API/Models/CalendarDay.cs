﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;

namespace DateTimeBombs.API.Models
{
    public enum Month
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November =  11,
        December = 12
    }

    public interface ICalendarDay
    {
        short Year { get; set; }
        Month Month { get; set; }
        byte Day { get; set; }
    }

    [TypeConverter(typeof(CalendarDayTypeConverter ))]
    public struct CalendarDay: ICalendarDay
    {
        public short Year { get; set; }
        public Month Month { get; set; }
        public byte Day { get; set; }

        public CalendarDay(
            short year,
            Month month,
            byte day)
        {
            Year = year;
            Month = month;
            Day = day;
        }
    }

    class CalendarDayTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, 
            CultureInfo culture, object value)
        {
            if (!(value is string)) 
                return base.ConvertFrom(context, culture, value);
            var val = ((string) value);
            const string pattern = @"(?<month>\d{2})[-\/](?<day>\d{2})[-\/](?<year>\d{4})|(?<year>\d{4})[-\/](?<month>\d{2})[-\/](?<day>\d{2})";

            try
            {
                var matches = Regex.Matches(val, pattern);
                
                if (matches.Count == 0 || matches[0].Groups.Count == 0)
                {
                    return base.ConvertFrom(context, culture, value);
                }

                var strMonth = matches[0].Groups["month"].ToString();
                var strDay = matches[0].Groups["day"].ToString();
                var strYear = matches[0].Groups["year"].ToString();

                var m = 0;
                byte day = 0;
                var succeeded = short.TryParse(strYear, out var year) && int.TryParse(strMonth, out m) &&
                                byte.TryParse(strDay, out day);
                var month = (Month)m;

                return succeeded ? new CalendarDay(year, month, day) : base.ConvertFrom(context, culture, value);
            }
            catch
            {
                return base.ConvertFrom(context, culture, value);
            }

        }
    }
}