﻿using System;
using NodaTime;
using NodaTime.Extensions;

namespace DateTimeBombs.API.Models
{
    public class TimeCardBCL
    {
 
        public DateTime PostingDate { get; set; }
        public DateTime ClockIn { get; set; }
        public DateTime ClockOut { get; set; }

        public TimeSpan Duration => ClockOut - ClockIn;
    }
}