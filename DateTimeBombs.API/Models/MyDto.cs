﻿using System;
using System.Collections.Generic;

namespace DateTimeBombs.API.Models
{
    public class MyDto
    {
        public CalendarDay Day;
        public DateTime DateTime;
        public string SomeString;
        public IEnumerable<string> ListOfStrings;
    }
}