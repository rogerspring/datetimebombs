﻿using System.Web.Http;
using System.Web.Http.Cors;
using NodaTime;

namespace DateTimeBombs.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public abstract class ApiBaseController : ApiController
    {
        protected IClock AppClock;

        protected ApiBaseController(IClock appClock)
        {
            AppClock = appClock;
        }
    }
}
