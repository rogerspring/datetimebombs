﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using NodaTime;

namespace DateTimeBombs.API.Controllers
{	
    [RoutePrefix("cancelable")]
    public class CancelableController : ApiBaseController
    {
        public CancelableController(IClock appClock) : base(appClock)
        {
        }

        [HttpGet, Route("")]
        public Task<string> Get(DateTime date, CancellationToken cancellationToken = default(CancellationToken))
        {
            
            return GetDateAsync(date, cancellationToken);
        }

        private Task<string> GetDateAsync(DateTime date, CancellationToken cancellationToken)
        {
            try
            {
                var loopCount = 0;

                while (loopCount < 5)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                    }

                    Debug.WriteLine($"Debug loopCount is {loopCount}");
                    Task.Delay(TimeSpan.FromSeconds(2));
                    ++loopCount;
                }
                var d = DateTime.SpecifyKind(date, DateTimeKind.Local);

                var result = d.ToLocalTime().ToString(CultureInfo.InvariantCulture);

                Debug.WriteLine($"returning {result}");
                return Task.FromResult(result);
            }
            catch (OperationCanceledException)
            {
                Debug.WriteLine("Request was cancelled.");
                return null;
            }
        }
    }
}
