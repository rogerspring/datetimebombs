﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Http;
using System.Web.ModelBinding;
using DateTimeBombs.API.Models;
using DateTimeBombs.NodaTimeDateTimeProvider.Utilities;
using NodaTime;

namespace DateTimeBombs.API.Controllers
{	
    [RoutePrefix("dates")]
    public class DatesController : ApiBaseController
    {
        public DatesController(IClock appClock) : base(appClock)
        {
        }

        [Route("current-time")]
        [HttpGet]
        public DateTime CurrentTime()
        {
            var today = DateTime.Today;
            return today;
        }

        [Route("")]
        [HttpGet]
        public DateTime Headless(DateTime date)
        {
            return date.ToUniversalTime().Date;
        }

        [Route("before-today")]
        [HttpGet]
        public bool BeforeToday(DateTime date)
        {
            var result = date.Date < DateTime.UtcNow.Date;
            return result;
        }

        [Route("Serialize")]
        [HttpPost]
        public DateTime Serialize(TimeCardBCL date)
        {
            return date.PostingDate.ToUniversalTime();
        }

        [HttpGet, Route("ParseCalendarDay")]
        public DateTime ParseCalendarDay(CalendarDay calendarDay)
        { 
            var date = new DateTime(calendarDay.Year, (int)calendarDay.Month, calendarDay.Day);
            //var d = DateTime.SpecifyKind(date, DateTimeKind.Local);
            return date;
        }

        [HttpGet, Route("ParseCalendarDayAsRoute/{calendarDay}")]
        public DateTime ParseCalendarDayAsRoute(CalendarDay calendarDay)
        { 
            var date = new DateTime(calendarDay.Year, (int)calendarDay.Month, calendarDay.Day);
            //var d = DateTime.SpecifyKind(date, DateTimeKind.Local);
            return date;
        }



        [HttpGet, Route("TodayAsCalendarDay")]
        public ICalendarDay GetTodayAsCalendarDay()
        {
            var year = (short) DateTime.Today.Year;
            var month = (Month) DateTime.Today.Month;
            var day = (byte) DateTime.Today.Day;
            var today = new CalendarDay(year, month, day);
            return today;
        }

        [HttpGet, Route("ObjectsWithCalendarDay")]
        public MyDto GetObjectsWithCalendarDay()
        {

            var year = (short) DateTime.Today.Year;
            var month = (Month) DateTime.Today.Month;
            var day = (byte) DateTime.Today.Day;
            var today = new CalendarDay(year, month, day);

            var myDto = new MyDto
            {
                DateTime = DateTime.UtcNow,
                Day = today,
                SomeString = "blah blah",
                ListOfStrings = new List<string> {"Foo", "Bar", "Baz"}
            };
            return myDto;
        }

        [HttpPost, Route("ObjectsWithCalendarDay")]
        public void Post([FromBody]MyDto myDto)
        {
            var day = myDto.Day;
            var date = myDto.DateTime;
            Debug.WriteLine(day);
            Debug.WriteLine(date);
        }

        [HttpGet, Route("IsTodayFederalHoliday")]
        public bool IsTodayFederalHoliday(string zoneName = "America/Los_Angeles")
        {
            DateTimeZone zone = DateTimeZoneProviders.Tzdb.GetZoneOrNull(zoneName);
            Instant now = AppClock.GetCurrentInstant();
            var local = now.InZone(zone).LocalDateTime;
            return local.IsFederalHoliday();
        }

        [Route("submit-time-card")]
        [HttpPost]
        public TimeCardBCL SubmitTimeCard(TimeCardBCL timeCardBCL)
        {
            return timeCardBCL;
        }

        [Route("as-utc")]
        [HttpGet]
        public object asUTC([QueryString] DateTimeOffset dateTime)
        {
            return new
            {
                entered = dateTime,
                utc = dateTime.ToUniversalTime()
            };
        }
    }
}
