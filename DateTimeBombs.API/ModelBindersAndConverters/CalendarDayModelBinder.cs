﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using DateTimeBombs.API.Models;

namespace DateTimeBombs.API.ModelBindersAndConverters
{
    //https://stackoverflow.com/questions/10293440/how-to-make-asp-net-mvc-model-binder-treat-incoming-date-as-utc
    public class CalendarDayModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException(nameof(bindingContext));

            var stringValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName)?.AttemptedValue;

            if (bindingContext.ModelType == typeof(CalendarDay?) && string.IsNullOrEmpty(stringValue))
            {
                return true;
            }
            var m = 0;
            byte day = 0;

            var values = stringValue.Split('-');
            var succeeded = short.TryParse(values[0], out var year) && int.TryParse(values[1], out m) &&
                byte.TryParse(values[2], out day);
            var month = (Month) m;

            var result = new CalendarDay(year, month, day);
            bindingContext.Model = result;
                
            return succeeded;
        }
        
    }
}