﻿using System;
using System.Globalization;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace DateTimeBombs.API.ModelBindersAndConverters
{
    //https://stackoverflow.com/questions/10293440/how-to-make-asp-net-mvc-model-binder-treat-incoming-date-as-utc
    public class DateTimeModelBinder : IModelBinder
    {
        private static readonly string[] DateTimeFormats =
            {"yyyyMMdd'T'HHmmss.FFFFFFFK", "yyyy-MM-dd'T'HH:mm:ss.FFFFFFFK"};

        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException(nameof(bindingContext));

            var stringValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName)?.AttemptedValue;

            if (bindingContext.ModelType == typeof(DateTime?) && string.IsNullOrEmpty(stringValue))
            {
                return true;
            }

            var succeeded = DateTime.TryParseExact(stringValue, DateTimeFormats, CultureInfo.InvariantCulture,
                                                    DateTimeStyles.RoundtripKind, out var result);
            bindingContext.Model = result;
                
            return succeeded;
        }
        
    }
}