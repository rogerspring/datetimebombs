﻿using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using NodaTime;
using NodaTime.Text;

namespace DateTimeBombs.API.ModelBindersAndConverters
{
    public class LocalDateModelBinder : IModelBinder
    {
        private readonly LocalDatePattern _localDatePattern = LocalDatePattern.Iso;

        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof (LocalDate))
                return false;

            ValueProviderResult val = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (val == null)
                return false;

            var rawValue = val.RawValue as string;

            var result = _localDatePattern.Parse(rawValue);
            if (result.Success)
                bindingContext.Model = result.Value;

            return result.Success;
        }
    }
}