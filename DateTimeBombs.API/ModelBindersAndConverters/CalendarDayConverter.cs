﻿using System;
using System.IO;
using DateTimeBombs.API.Models;
using Newtonsoft.Json;

namespace DateTimeBombs.API.ModelBindersAndConverters
{
    public class CalendarDayConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(CalendarDay) || objectType == typeof(CalendarDay?);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var calendarDay = (CalendarDay)value;

            writer.WriteStartObject();

            //Build as object with separate properties

            writer.WritePropertyName("Year");
            serializer.Serialize(writer, calendarDay.Year);
            writer.WritePropertyName("Month");
            serializer.Serialize(writer, calendarDay.Month);
            writer.WritePropertyName("Day");
            serializer.Serialize(writer, calendarDay.Day);

            // ---------- OR ------------

            //Build as ISO date format
            //writer.WritePropertyName("CalendarDay");
            //serializer.Serialize(writer, $"{calendarDay.Year}-{(int)calendarDay.Month:D2}-{calendarDay.Day:D2}");
            
            
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var year = default(short);
            var month = default(Month);
            var day = default(byte);
            var gotYear = false;
            var gotMonth = false;
            var gotDay = false;
            while (reader.Read())
            {
                if (reader.TokenType != JsonToken.PropertyName)
                    break;

                var propertyName = (string)reader.Value;
                if (!reader.Read())
                    continue;

                switch (propertyName)
                {
                    case "Year":
                        year = serializer.Deserialize<short>(reader);
                        gotYear = true;
                        break;
                    case "Month":
                        month = serializer.Deserialize<Month>(reader);
                        gotMonth = true;
                        break;
                    case "Day":
                        day = serializer.Deserialize<byte>(reader);
                        gotDay = true;
                        break;
                }
            }

            if (!(gotYear && gotMonth && gotDay))
            {
                throw new InvalidDataException("An CalendarDay must contain Year, Month, and Day properties.");
            }

            return new CalendarDay(year, month, day);
        }
    }
}