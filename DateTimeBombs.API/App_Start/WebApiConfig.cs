﻿using System;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.ModelBinding.Binders;
using DateTimeBombs.API.ModelBindersAndConverters;
using DateTimeBombs.NodaTimeDateTimeProvider;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NodaTime;
using NodaTime.Serialization.JsonNet;
using RailPros.Api.App_Start;
using Unity;
using Unity.WebApi;

namespace DateTimeBombs.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.EnableCors();

            // Web API configuration and services
            UnityContainerConfig(config);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            
            JsonMediaTypeFormatter configJsonFormatter = config.Formatters.JsonFormatter;

            configJsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            configJsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();


            //// Setup provider to bind datetime params as UTC
            var dateTimeProvider = new SimpleModelBinderProvider(typeof(DateTime), new DateTimeModelBinder());
            config.Services.Insert(typeof(ModelBinderProvider), 0, dateTimeProvider);


            //// Add De/Serializer for custom CalendarDay type.
            //var calendarDayProvider = new SimpleModelBinderProvider(typeof(DateTime), new CalendarDayModelBinder());
            //config.Services.Insert(typeof(ModelBinderProvider), 0, calendarDayProvider);
            //// Inspired by https://stackoverflow.com/questions/24726273/why-can-i-not-deserialize-this-custom-struct-using-json-net
            //configJsonFormatter.SerializerSettings.Converters.Add(new CalendarDayConverter());
            
            //// Add De/Serializer for custom LocalDate type.
            //var localDateProvider = new SimpleModelBinderProvider(typeof(LocalDate), new LocalDateModelBinder());
            //config.Services.Insert(typeof(ModelBinderProvider), 0, localDateProvider);

            //configJsonFormatter.UseDataContractJsonSerializer = false;


            configJsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            
            //configJsonFormatter.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
            
            // NodaTimeSettings
            //configJsonFormatter.SerializerSettings.ConfigureForNodaTime(GetDateTimeZoneProvider());
            
            SwaggerConfig.Register(config, configJsonFormatter.SerializerSettings);

        }
        private static IDateTimeZoneProvider GetDateTimeZoneProvider()
        {
            var dateTimeZoneProvider = DateTimeZoneProviders.Tzdb;
            return dateTimeZoneProvider;
        }

        private static void UnityContainerConfig(HttpConfiguration config)
        {
            // Unity configuration
            var container = new UnityContainer();
            container.RegisterType<IClock, AppDateTimeProvider>();
            //var clock = container.Resolve<IClock>();
            //container.RegisterType<CancelableController>(new InjectionConstructor(clock));
            //container.RegisterType<DatesController>(new InjectionConstructor(clock));

            config.DependencyResolver = new UnityResolver(container);

            //this
            config.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
