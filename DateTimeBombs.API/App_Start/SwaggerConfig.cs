using System.Web;
using System.Web.Http;
using DateTimeBombs.API;
using Newtonsoft.Json;
using Swashbuckle.Application;
using Swashbuckle.NodaTime;

//[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace DateTimeBombs.API
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration httpConfiguration,
            JsonSerializerSettings jsonSerializerSettings)
        {
            httpConfiguration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "DateTimeBombs.API");
                    // Added this here to configure swagger with nodatime
                    //c.ConfigureForNodaTime(jsonSerializerSettings);
                    c.DescribeAllEnumsAsStrings();
                })
                .EnableSwaggerUi();
        }
    }
}
