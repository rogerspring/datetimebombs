using FluentAssertions;
using FluentAssertions.Execution;
using DateTimeBombs.BCL.Groceries;
using DateTimeBombs.BCL.Untestables.Groceries;
using System;
using Xunit;
using Xunit.Abstractions;

namespace DateTimeBombs.BCL.Tests.Untestables
{
    public class FridgeTests
    {
        private readonly ITestOutputHelper _outputHelper;

        public FridgeTests(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public void IsTimeToCleanTheFridge_WithExpiredItems_ReturnsTrue()
        {
            var purchaseDateTime = DateTime.Now;
            var sut = new BCL.Untestables.Fridge();

            sut.StockFridge(new IGroceryItem[]
            {
                new BCL.Untestables.Groceries.Milk(),
                new BCL.Untestables.Groceries.Ketchup()
            });

            using (new AssertionScope())
            {
                // While we can test for False immediately, because virtuallly no time will have
                // passed since we stocked our fridge and when we're executing this command.
                sut.IsTimeToCleanTheFridge().Should().BeFalse();

                // The only way to test this method ever returns true is to wait 7 days.
            }
        }

    }
}