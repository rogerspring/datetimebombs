﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace DateTimeBombs.BCL.Tests
{
    [TestFixture]
    public class DateTimeQuarterExtensionsTests
    {
        //[TestCaseSource(typeof(DateDataClass), nameof(DateDataClass.Dates))]
        //public int GetQuarterOneLiner( DateTime date)
        //{
        //    return date.GetQuarterOneLiner();
        //}

        //[TestCaseSource(typeof(DateDataClass), nameof(DateDataClass.Dates))]
        //public int GetQuarterBranched(DateTime date)
        //{
        //    return date.GetQuarterBranched();
        //}

        //[TestCaseSource(typeof(DateDataClass), nameof(DateDataClass.Dates))]
        //public int GetQuarterConcise(DateTime date)
        //{
        //    return date.GetQuarterConcise();
        //}

      
        //[TestCaseSource(typeof(DateDataClass), nameof(DateDataClass.Dates))]
        //public int GetQuarterSimplest(DateTime date)
        //{
        //    return date.GetQuarterSimplest();
        //}

        //[TestCaseSource(typeof(DateFiscalReturnDataClass), nameof(DateFiscalReturnDataClass.Dates))]
        //public int GetFiscalQuarterSimplest(DateTime date)
        //{
        //    return date.GetFiscalQuarterSimplest();
        //}

        [SuppressMessage("ReSharper", "UnusedMember.Local")]
        private class DateDataClass
        {
            public static IEnumerable Dates
            {
                get
                {
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 01, 01), DateTimeKind.Utc)).Returns(1);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 02, 01), DateTimeKind.Utc)).Returns(1);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 03, 01), DateTimeKind.Utc)).Returns(1);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 04, 01), DateTimeKind.Utc)).Returns(2);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 05, 01), DateTimeKind.Utc)).Returns(2);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 06, 01), DateTimeKind.Utc)).Returns(2);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 07, 01), DateTimeKind.Utc)).Returns(3);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 08, 01), DateTimeKind.Utc)).Returns(3);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 09, 01), DateTimeKind.Utc)).Returns(3);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 10, 01), DateTimeKind.Utc)).Returns(4);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 11, 01), DateTimeKind.Utc)).Returns(4);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 12, 01), DateTimeKind.Utc)).Returns(4);
                }
            }  
        }
        
        [SuppressMessage("ReSharper", "UnusedMember.Local")]
        private class DateFiscalReturnDataClass
        {
            public static IEnumerable Dates
            {
                get
                {
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 01, 01), DateTimeKind.Utc)).Returns(4);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 02, 01), DateTimeKind.Utc)).Returns(4);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 03, 01), DateTimeKind.Utc)).Returns(4);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 04, 01), DateTimeKind.Utc)).Returns(1);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 05, 01), DateTimeKind.Utc)).Returns(1);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 06, 01), DateTimeKind.Utc)).Returns(1);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 07, 01), DateTimeKind.Utc)).Returns(2);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 08, 01), DateTimeKind.Utc)).Returns(2);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 09, 01), DateTimeKind.Utc)).Returns(2);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 10, 01), DateTimeKind.Utc)).Returns(3);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 11, 01), DateTimeKind.Utc)).Returns(3);
                    yield return new TestCaseData(DateTime.SpecifyKind(new DateTime(2010, 12, 01), DateTimeKind.Utc)).Returns(3);
                }
            }  
        }

    }
}
