﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using NPOI.SS.UserModel;
using Xunit.Sdk;

namespace XUnitTheoryTests
{
    public class ExcelFileDataAttribute : DataAttribute
    {
        private readonly string _filePath;
        private readonly string _propertyName;

        /// <summary>
        /// Load data from a JSON file as the data source for a theory
        /// </summary>
        /// <param name="filePath">The absolute or relative path to the JSON file to load</param>
        public ExcelFileDataAttribute(string filePath)
            : this(filePath, null) { }

        /// <summary>
        /// Load data from a JSON file as the data source for a theory
        /// </summary>
        /// <param name="filePath">The absolute or relative path to the JSON file to load</param>
        /// <param name="propertyName">The name of the property on the JSON file that contains the data for the test</param>
        public ExcelFileDataAttribute(string filePath, string propertyName = null)
        {
            _filePath = filePath;
            _propertyName = propertyName;
        }

        /// <inheritDoc />
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            if (testMethod == null) { throw new ArgumentNullException(nameof(testMethod)); }



            // Get the absolute path to the JSON file
            var path = Path.IsPathRooted(_filePath)
                ? _filePath
                : Path.GetRelativePath(Directory.GetCurrentDirectory(), _filePath);

            if (!File.Exists(path))
            {
                throw new ArgumentException($"Could not find file at path: {path}");
            }

            IWorkbook workbook;
            using (var file = new FileStream(_filePath, FileMode.Open, FileAccess.Read))
            {
                workbook = WorkbookFactory.Create(file);
            }

            ISheet sheetOfInterest = string.IsNullOrWhiteSpace(_propertyName) ? workbook.GetSheetAt(0) : workbook.GetSheet(_propertyName);

            var rows = new List<object[]>();
            foreach (IRow row in sheetOfInterest)
            {
                var columns = new List<object>();
                for (var i = 0; i < row.LastCellNum; i++)
                {
                    ICell cell = row.GetCell(i);
                    if (cell == null) {
                        columns.Add(null);
                    }
                    else
                    {
                        columns.Add(cell.NumericCellValue);
                    }
                }
                rows.Add(columns.ToArray());
            }

            return rows;

        }
    }
}
