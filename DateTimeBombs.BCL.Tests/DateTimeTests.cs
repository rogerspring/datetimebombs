﻿using System;
using NUnit.Framework;
using FluentAssertions;

namespace DateTimeBombs.BCL.Tests
{
    [TestFixture]
    public class DateTimeTests
    {
        [Test]
        public void DateTimeOffset_WithStoredUTCDatesAndPickedWithOffsetTime_CausesFalseNegative()
        {

            // Dates picked from a UI running in America/New_York time zone
            DateTimeOffset startDate = new DateTimeOffset(new DateTime(2020, 2, 03, 5, 0, 0), new TimeSpan(0, 0, 0));
            DateTimeOffset endDate = new DateTimeOffset(new DateTime(2020, 2, 28, 5, 0, 0), new TimeSpan(0, 0, 0));


            // Date picked from a UI running in America/Los_Angeles time zone
            DateTimeOffset localPickedDate = new DateTimeOffset(new DateTime(2020, 2, 28), new TimeSpan(-08, 0, 0));
            
            var result = localPickedDate >= startDate && localPickedDate <= endDate;

            // Should actually be true, but an incoming date with the 'local' offset attached, pushed the date outside the tested range
            result.Should().BeFalse();
        }

        [Test]
        public void DateTimeOffset_WithStoredUTCDatesAndPickedWithUTC_DoNotRequireDateProperty()
        {
            DateTimeOffset startDate = new DateTimeOffset(new DateTime(2020, 2, 03, 0, 0, 0), new TimeSpan(0, 0, 0));
            DateTimeOffset endDate = new DateTimeOffset(new DateTime(2020, 2, 28, 0, 0, 0), new TimeSpan(0, 0, 0));

            DateTimeOffset localPickedDate = new DateTimeOffset(new DateTime(2020, 2, 28), new TimeSpan(-08, 0, 0));

            // Though the stored values are UTC midnight, this is still false because of incoming offset.
            var resultWithoutDate = localPickedDate >= startDate && localPickedDate <= endDate;
            resultWithoutDate.Should().BeFalse();

            Console.WriteLine($"Converted to UTC {localPickedDate.ToUniversalTime()}");
            var resultToUniversal = localPickedDate.ToUniversalTime() >= startDate && localPickedDate.ToUniversalTime() <= endDate;
            resultToUniversal.Should().BeFalse();


            // Any guesses on what DateTimeKind this DateTime will be?
            DateTime localDateOnlyPicked = localPickedDate.ToUniversalTime().Date;
            Console.WriteLine($"Date picked: { localDateOnlyPicked }");
            Console.WriteLine($"DateTimeKind: { localDateOnlyPicked.Kind }");


            // Because `.Date` results in a DateTime with Unspecified Kind, comparing to a DateTimeOffset = return false. Using the `.Date` property should not be necessary here.
            var resultWithDate = localPickedDate.Date >= startDate && localPickedDate.Date <= endDate;
            resultWithDate.Should().BeFalse();

            // It becomes necessary to use the `.Date` property on both data points.
            var resultComparingDate = localPickedDate.Date >= startDate.Date && localPickedDate.Date <= endDate.Date;
            resultComparingDate.Should().BeTrue();

        }

        [Test]
        public void DateTimeOffset_WithStoredUTCDatesAndPickedWithOffsetTime_RequiresDateProperty()
        {
            DateTimeOffset localPickedDate = new DateTimeOffset(new DateTime(2020, 2, 28), new TimeSpan(-08, 0, 0));

            DateTimeOffset startDate = new DateTimeOffset(new DateTime(2020, 2, 03, 8, 0, 0), new TimeSpan(0, 0, 0));
            DateTimeOffset endDate = new DateTimeOffset(new DateTime(2020, 2, 28, 8, 0, 0), new TimeSpan(0, 0, 0));
            
            var result = localPickedDate.Date >= startDate.Date && localPickedDate.Date <= endDate.Date;

            // Using the `.Date` property should not be necessary here.
            result.Should().BeTrue();
        }
    }
}
