using FluentAssertions;
using FluentAssertions.Execution;
using DateTimeBombs.BCL.Groceries;
using System;
using Xunit;
using Xunit.Abstractions;

namespace DateTimeBombs.BCL.Tests
{
    public class FridgeTests
    {
        private readonly ITestOutputHelper _outputHelper;

        public FridgeTests(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public void IsTimeToCleanTheFridge_WithExpiredItems_ReturnsTrue()
        {
            FakeClock fakeClock = FakeClock.FromUtc(2019, 01, 01);
            var startDateTime = GetUtcDate(fakeClock);

            var sut = new Fridge(fakeClock);

            sut.StockFridge(new IGroceryItem[]
            {
                new CottageCheese(startDateTime),
                new Milk(startDateTime),
                new Ketchup(startDateTime)
            });

            using (new AssertionScope())
            {
                sut.IsTimeToCleanTheFridge().Should().BeFalse();

                fakeClock.Advance(TimeSpan.FromDays(5));
                sut.IsTimeToCleanTheFridge().Should().BeFalse();

                fakeClock.Advance(TimeSpan.FromDays(5));
                sut.IsTimeToCleanTheFridge().Should().BeTrue();
            }

        }        
        
        [Fact]
        public void NumberOfExpiredItems_WithExpiredItems_ReturnsCorrectCount()
        {
            var fakeClock = FakeClock.FromUtc(2019, 01, 01);
            var startDateTime = GetUtcDate(fakeClock);

            var sut = new Fridge(fakeClock);

            sut.StockFridge(new IGroceryItem[]
            {
                new CottageCheese(startDateTime),
                new Milk(startDateTime),
                new Ketchup(startDateTime)
            });

            using (new AssertionScope())
            {
                sut.NumberOfExpiredItems().Should().Be(0);

                fakeClock.Advance(TimeSpan.FromDays(9));
                _outputHelper.WriteLine($"Days since stocked fridge {(GetUtcDate(fakeClock) - startDateTime).Days}");
                _outputHelper.WriteLine($"Number Of Expired Items: {sut.NumberOfExpiredItems()}");
                sut.NumberOfExpiredItems().Should().Be(1);

                fakeClock.Advance(TimeSpan.FromDays(3));
                _outputHelper.WriteLine($"Days since stocked fridge {(GetUtcDate(fakeClock) - startDateTime).Days}");
                _outputHelper.WriteLine($"Number Of Expired Items: {sut.NumberOfExpiredItems()}");
                sut.NumberOfExpiredItems().Should().Be(2);

                fakeClock.Advance(TimeSpan.FromDays(170));
                _outputHelper.WriteLine($"Days since stocked fridge {(GetUtcDate(fakeClock) - startDateTime).Days}");
                _outputHelper.WriteLine($"Number Of Expired Items: {sut.NumberOfExpiredItems()}");
                sut.NumberOfExpiredItems().Should().Be(2);

                fakeClock.Advance(TimeSpan.FromDays(1));
                _outputHelper.WriteLine($"Days since stocked fridge {(GetUtcDate(fakeClock) - startDateTime).Days}");
                _outputHelper.WriteLine($"Number Of Expired Items: {sut.NumberOfExpiredItems()}");
                sut.NumberOfExpiredItems().Should().Be(3);
            }
        }

        private static DateTime GetUtcDate(FakeClock fakeClock)
        {
            return new DateTime(fakeClock.GetCurrentTicks(), DateTimeKind.Utc);
        }
    }
}