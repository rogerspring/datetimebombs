﻿using System;

namespace DateTimeBombs.BCL.Tests
{
    public class FakeClock : IClock
    {
        private readonly object mutex = new object();
        private long _currentTicks;

        private FakeClock(int year, int month, int day, int? hour = 0, int? minute = 0, int? second = 0)
        { 
            _currentTicks = new DateTime(year, month, day, hour.Value, minute.Value, second.Value, DateTimeKind.Utc).Ticks;
        }

        public long GetCurrentTicks() => _currentTicks;

        public static FakeClock FromUtc(int year, int month, int day, int? hour = 0, int? minute = 0, int? second = 0)
        {
            return new FakeClock(year, month, day, hour, minute, second);
        }

        /// <summary>
        /// Advances the ticks by the amount the timespan ticks. Can be negative to move time backwards.
        /// </summary>
        public void Advance(TimeSpan timeSpan)
        {
            lock (mutex) {
                _currentTicks += timeSpan.Ticks;
            }
        }

        public void AdvanceMilliseconds(long milliseconds) {
            Advance(TimeSpan.FromMilliseconds(milliseconds));
        }

        public void AdvanceSeconds(long seconds) {
            Advance(TimeSpan.FromSeconds(seconds));
        }

        public void AdvanceMinutes(int minutes) {
            Advance(TimeSpan.FromMinutes(minutes));
        }

        public void AdvanceHours(int hours) {
            Advance(TimeSpan.FromHours(hours));
        }

        public void AdvanceDays(int days) {
            Advance(TimeSpan.FromDays(days));
        }

        public void Reset(long ticks)
        {
            lock (mutex) {
                _currentTicks = ticks;
            }
        }
    }
}
