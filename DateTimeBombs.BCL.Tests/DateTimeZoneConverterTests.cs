using System;
using NUnit.Framework;

namespace DateTimeBombs.BCL.Tests
{
    [TestFixture]
    public class DateTimeZoneConverterTests
    {
        [TestCase("2019-05-23T05:30:00 Central Standard Time")]
        public void ExtractTimeZoneName_returns_time_zone(string dateTimeZoneString)
        {
            var actual = DateTimeZoneConverter.ExtractTimeZoneName(dateTimeZoneString);

            Assert.That(actual, Is.EqualTo("Central Standard Time"));
        }

        [TestCase("2019-05-23T05:30:00 Eastern Standard Time", -4)]
        [TestCase("2019-05-23T05:30:00 Central Standard Time", -5)]
        [TestCase("2019-05-23T05:30:00 Mountain Standard Time", -6)]
        [TestCase("2019-05-23T05:30:00 US Mountain Standard Time", -7)]
        [TestCase("2019-05-23T05:30:00 Pacific Standard Time", -7)]
        [TestCase("2019-05-23T05:30:00Z", 0)]
        [TestCase("2019-05-23T05:30:00-07:00", -7)]
        public void ToDateTimeOffset_can_convert(string dateTimeZoneString, int offset)
        {



            var actual = DateTimeZoneConverter.ToDateTimeOffset(dateTimeZoneString);

            Assert.That(actual, Is.EqualTo(new DateTimeOffset(2019, 5, 23, 5, 30, 0, TimeSpan.FromHours(offset))));
        }
    }
}