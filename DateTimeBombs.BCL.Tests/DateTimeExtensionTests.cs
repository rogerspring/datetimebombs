﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using DateTimeBombs.BCL;
using FluentAssertions;

namespace DateTimeBombs.BCL.Tests
{
    [TestFixture]
    public class DateTimeExtensionsTests
    {
        [Test]
        public void LastDayOfMonth_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.LastDayOfMonth();

            // Assert
            Assert.Fail();
        }

        [TestCase(1, 1, 15)]
        [TestCase(1, 15, 15)]
        [TestCase(1, 16, 31)]
        [TestCase(1, 31, 31)]
        public void GetNextBiMonthlyEndDate_FromFirstHalfOfMonth_ReturnsMidMonth(int month, int day, int expected)
        {
            // Arrange
            DateTime date = new DateTime(2019, month, day);

            // Act
            var actual = date.GetNextBiMonthlyEndDate();

            // Assert
            Assert.That(actual.Day, Is.EqualTo(expected));
        }

        [Test]
        public void GetQuarter_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.GetQuarter();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void GetNextQuarter_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.GetNextQuarter();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void GetQuarterStartDate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.GetQuarterStartDate();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void GetQuarterEndDate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.GetQuarterEndDate();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void GetNextQuarterStartDate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.GetNextQuarterStartDate();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void GetNextQuarterEndDate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.GetNextQuarterEndDate();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void GetSixMonthPeriodEnding_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.GetSixMonthPeriodEnding();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void YearEndDate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.YearEndDate();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void YearBeginDate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2019, 1, 1);

            // Act
            var result = date.YearBeginDate();

            // Assert
            Assert.Fail();
        }

        [Test]
        public void GetNextWeekday_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2020, 1, 1);

            // Act
            DateTime result = date.GetNextWeekday(DayOfWeek.Sunday);

            // Assert
            result.Year.Should().Be(2020);
            result.Month.Should().Be(1);
            result.Day.Should().Be(5);
        }

        [Test]
        public void GetNextWeekEndingDate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2020, 1, 1);

            // Act
            DateTime result = date.GetNextWeekEndingDate();

            // Assert
            result.Year.Should().Be(2020);
            result.Month.Should().Be(1);
            result.Day.Should().Be(4);
        }

        [Test]
        public void GetPreviousWeekEndingDate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            DateTime date = new DateTime(2020, 1, 1);

            // Act
            DateTime result = date.GetPreviousWeekEndingDate();

            // Assert
            result.Year.Should().Be(2019);
            result.Month.Should().Be(12);
            result.Day.Should().Be(29);
        }

        [TestCase(EnumFrequency.Annually, "2022-12-31")]
        [TestCase(EnumFrequency.SemiAnnual, "2021-06-30")]
        [TestCase(EnumFrequency.Quarterly, "2020-09-30")]
        [TestCase(EnumFrequency.Monthly, "2020-03-31")]
        [TestCase(EnumFrequency.BiMonthly, "2020-02-15")]
        [TestCase(EnumFrequency.Weekly, "2020-01-18")]
        [TestCase(EnumFrequency.Daily, "2020-01-3")]
        public void NextFrequencyDates_LimitedByCount_ExpectedBehavior(EnumFrequency frequency, DateTime expected)
        {
            // Arrange
            DateTime date = new DateTime(2020, 1, 1);

            // Act
            var result = date.NextPeriodicDates(frequency, 3).ToArray();
            
            // Assert
            result.Length.Should().Be(3);
            result[2].Year.Should().Be(expected.Year);
            result[2].Month.Should().Be(expected.Month);
            result[2].Day.Should().Be(expected.Day);
        }

        
        [TestCase(EnumFrequency.Annually, "2023-01-01", "2022-12-31")]
        [TestCase(EnumFrequency.SemiAnnual, "2021-07-01", "2021-06-30")]
        [TestCase(EnumFrequency.Quarterly, "2020-10-01", "2020-09-30")]
        [TestCase(EnumFrequency.Monthly, "2020-04-01", "2020-03-31")]
        [TestCase(EnumFrequency.BiMonthly, "2020-02-16", "2020-02-15")]
        [TestCase(EnumFrequency.Weekly, "2020-01-19", "2020-01-18")]
        [TestCase(EnumFrequency.Daily, "2020-01-04", "2020-01-03")]
        public void NextFrequencyDates_LimitedByDate_ExpectedBehavior(EnumFrequency frequency, DateTime until, DateTime expected)
        {
            // Arrange
            DateTime date = new DateTime(2020, 1, 1);

            // Act
            var result = date.NextPeriodicDates(frequency, until).ToArray();
            
            // Assert
            result.Length.Should().Be(3);
            result[2].Year.Should().Be(expected.Year);
            result[2].Month.Should().Be(expected.Month);
            result[2].Day.Should().Be(expected.Day);
        }
        
        [TestCase(EnumFrequency.Annually, "2023-01-01")]
        [TestCase(EnumFrequency.SemiAnnual, "2021-07-01")]
        [TestCase(EnumFrequency.Quarterly, "2020-10-01")]
        [TestCase(EnumFrequency.Monthly, "2020-04-01")]
        [TestCase(EnumFrequency.BiMonthly, "2020-02-16")]
        [TestCase(EnumFrequency.Weekly, "2020-01-19")]
        [TestCase(EnumFrequency.Daily, "2020-01-04")]
        public void NumberOfPeriodicOccurrencesUntil_LimitedByDate_ShouldBeThree(EnumFrequency frequency, DateTime until)
        {
            // Arrange
            DateTime date = new DateTime(2020, 1, 1);

            // Act
            var result = date.NumberOfPeriodicOccurrencesUntil(frequency, until);
            
            // Assert
            result.Should().Be(3);
        }
        
        [Test]
        public void PerformanceTest()
        {
            // Arrange
            DateTime date = new DateTime(2020, 1, 1);

            // Act
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var result = date.NumberOfPeriodicOccurrencesUntil(EnumFrequency.Daily, new DateTime(3000, 1, 1));
            Console.WriteLine($"Calculated result of {result} in {sw.ElapsedMilliseconds}ms");
            sw.Restart();
            var result2 = date.NumberOfPeriodicOccurrencesUntil(EnumFrequency.Daily, new DateTime(3000, 1, 1));
            Console.WriteLine($"Retrieved cached result of {result2} in {sw.ElapsedMilliseconds}ms");
            sw.Restart();
            var result3 = date.NumberOfPeriodicOccurrencesUntil(EnumFrequency.Monthly, new DateTime(3000, 1, 1));
            Console.WriteLine($"Calculated result3 of {result3} in {sw.ElapsedMilliseconds}ms");

            // Assert
            result.Should().Be(result2);
        }
    }
}
