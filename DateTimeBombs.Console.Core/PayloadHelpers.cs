﻿using System.IO;
using System.Reflection;

namespace DateTimeBombs.CmdLn.Core
{
    public static class PayloadHelper
    {
        public static string LoadTextFromFile(string fileName, string subDirectory = null)
        {
            var filePath = GetFilePath(fileName, subDirectory);

            using (TextReader assignmentReader = new StreamReader(filePath))
            {
                return assignmentReader.ReadToEnd();
            }
        }

        public static string GetFilePath(string fileName, string subDirectory = null)
        {
            var appPath = GetAppPath();

            var folderPath = string.IsNullOrWhiteSpace(subDirectory) ? appPath : Path.Combine(appPath, subDirectory);
            var filePath = Path.Combine(folderPath, fileName);
            return filePath;
        }

        public static string GetAppPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var directoryName = Path.GetDirectoryName(codeBase);
            string appPath = directoryName?.Replace(@"file:\", string.Empty);
            // appPath = appPath.Replace(@"\bin\Debug", string.Empty);
            return appPath;
        }
    }
}
