﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace DateTimeBombs.CmdLn.Core
{
    class Program
    {
        static void Main(string[] args)
        {
            CurrentDateTimeAndKind();
            
            DateTimeToLocalAndUtc();

            // Change local time
            //DateTimeComparisons();

            //DateTimeKindExampleAndDeserializingData();

            //DateTimeDifferingKindsComparison();

            //DateTimeOffsetNowLessThanUtcNow();


        }

        private static void CurrentDateTimeAndKind()
        {		
            TimeZoneInfo localZone = TimeZoneInfo.Local;
            Console.WriteLine("Local Time Zone ID: {0}", localZone.Id);
            Console.WriteLine("   Display Name is: {0}.", localZone.DisplayName);
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine($"Current DateTime and Kind: {DateTimeAndKind(DateTime.Now)} in {localZone.DisplayName}");
            Console.WriteLine("______________________________________________________");
            Console.WriteLine(Environment.NewLine);

        }


        private static void DateTimeToLocalAndUtc()
        {
            var now = DateTime.Now;
            var uctNow = DateTime.UtcNow;
		
            Console.WriteLine($"DateTime.Now -----> {DateTimeAndKind(now)}");
            Console.WriteLine($"DateTime.UtcNow --> {DateTimeAndKind(uctNow)}");
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine($"DateTime.Now != DateTime.UtcNow = {now != uctNow}");
            Console.WriteLine($"DateTime.Now < DateTime.UtcNow = {now < uctNow}");
		
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine($"now.ToLocalTime() ------> {DateTimeAndKind(now.ToLocalTime())}");
            Console.WriteLine($"now.ToUniversalTime() --> {DateTimeAndKind(now.ToUniversalTime())}");
		
            Console.WriteLine(Environment.NewLine);
            var unspecified = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, DateTimeKind.Unspecified);
            Console.WriteLine($"unspecified.ToLocalTime() ------> {DateTimeAndKind(unspecified.ToLocalTime())}");
            Console.WriteLine($"unspecified.ToUniversalTime() --> {DateTimeAndKind(unspecified.ToUniversalTime())}");
        }

        /// <summary>
        /// Change local clock to some time after 5pm if in "America/Los Angeles" time zone
        /// </summary>
        private static void DateTimeComparisons()
        {
            Console.WriteLine($"DateTime.Today: {DateTime.Today}");
            Console.WriteLine($"DateTime.Today.ToUniversalTime(): {DateTime.Today.ToUniversalTime()}");
            Console.WriteLine($"DateTime.UtcNow.Date: {DateTime.UtcNow.Date}");
            Console.WriteLine($"DateTime.Today == DateTime.UtcNow.Date: {DateTime.Today == DateTime.UtcNow.Date}");
            Console.WriteLine($"DateTime.Now.Date == DateTime.UtcNow.Date: {DateTime.Now.Date == DateTime.UtcNow.Date}");
            Console.WriteLine($"DateTime.Now.Date < DateTime.UtcNow.Date: {DateTime.Now.Date < DateTime.UtcNow.Date}");
            Console.WriteLine($"DateTime.Now.Date > DateTime.UtcNow.Date: {DateTime.Now.Date > DateTime.UtcNow.Date}");
            //Console.WriteLine(
            //    $" local equals utc? {new DateTime(2021, 06, 01, 19, 00, 00, DateTimeKind.Local) == new DateTime(2021, 06, 02, 02, 00, 00, DateTimeKind.Utc)}");
        }

        private static void DateTimeKindExampleAndDeserializingData()
        {
            Transaction output;

            Console.WriteLine("Reading file...");
            string fileContent = PayloadHelper.LoadTextFromFile("transactions.json");
            
            output = JsonConvert.DeserializeObject<Transaction[]>(fileContent).First();
            Console.WriteLine($"Transaction DateTime.Kind: {output.Date.Kind}");

            Console.WriteLine(
                $"Convert {output.Date} with .ToLocalTime() with current system offset {DateTimeOffset.Now.Offset} --> {output.Date.ToLocalTime()}");
            Console.WriteLine(
                $"Convert {output.Date} with .ToUniversalTime() with current system offset {DateTimeOffset.Now.Offset} --> {output.Date.ToUniversalTime()}");

            #region With Serializer Settings
            Console.WriteLine("______________________________________________________");
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("With new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Utc }");
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Utc };
            output = JsonConvert.DeserializeObject<Transaction[]>(fileContent, serializerSettings).First();

            Console.WriteLine($"Transaction DateTime.Kind: {output.Date.Kind}");

            Console.WriteLine(
                $"Convert {output.Date} with .ToLocalTime() with current system offset {DateTimeOffset.Now.Offset} --> {output.Date.ToLocalTime()}");
            Console.WriteLine(
                $"Convert {output.Date} with .ToUniversalTime() with current system offset {DateTimeOffset.Now.Offset} --> {output.Date.ToUniversalTime()}");
            #endregion
        }
        
        private static void DateTimeDifferingKindsComparison()
        {
            DateTime local = new DateTime(2021, 06, 01, 12, 00, 00, DateTimeKind.Local);
            DateTime utc = new DateTime(2021, 06, 01, 12, 00, 00, DateTimeKind.Utc);
            Console.WriteLine($"DateTime (Local) --------------------------> {DateTimeAndKind(local)}");
            Console.WriteLine($"({local}).ToUniversalTime() --> {local.ToUniversalTime()}");
            Console.WriteLine($"DateTime (UTC): ---------------------------> {DateTimeAndKind(utc)}");
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine($"{DateTimeAndKind(local)} == {DateTimeAndKind(utc)}: {local == utc}");
            // result: 6/1/2021 12:00:00 PM (Local) == 6/1/2021 12:00:00 PM (Utc): True
            Console.WriteLine($"{DateTimeAndKind(local)} - {DateTimeAndKind(utc)}: {local - utc}");
            // result: 6/1/2021 12:00:00 PM (Local) - 6/1/2021 12:00:00 PM (Utc): 00:00:00
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine($"{DateTimeAndKind(local.ToUniversalTime())} - {DateTimeAndKind(utc)}: {local.ToUniversalTime() - utc}");
            // result: 6/1/2021 7:00:00 PM (Utc) - 6/1/2021 12:00:00 PM (Utc): 07:00:00

        }

        private static void DateTimeOffsetNowLessThanUtcNow()
        {


            Console.WriteLine($"DateTimeOffset.Now = {DateTimeOffset.Now}");
            Console.WriteLine($"DateTimeOffset.UtcNow = {DateTimeOffset.UtcNow}");
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine($"DateTimeOffset.Now < DateTimeOffset.UtcNow: {DateTimeOffset.Now < DateTimeOffset.UtcNow}");
        }

        
        private static string DateTimeAndKind(DateTime dateTime)
        {
            return $"{dateTime} (DateTimeKind.{dateTime.Kind})";
        }

        public class Transaction
        {
            public string Description { get; set; }
            public DateTime Date { get; set; }
            public decimal Amount { get; set; }
        }
    }
}
