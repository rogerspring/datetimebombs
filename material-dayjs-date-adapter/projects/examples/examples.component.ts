import * as dayjs from 'dayjs';
import * as utc from 'dayjs/plugin/utc.js';

import { Component, OnInit } from '@angular/core';
import { filter, map, startWith } from 'rxjs/operators';

import { DateAdapter } from '@angular/material/core';
import { Dayjs } from 'dayjs';
import { FormBuilder } from '@angular/forms';
import { FormConfig } from '../shared/forms/types';
import { UnsubscribeOnDestroy } from '../shared/utils/unsubscribe-ondestroy';

interface TestDateForm {
    datePicked: dayjs.Dayjs;
}

const testDateFormConfig: FormConfig<TestDateForm> = {
    datePicked: ['']
};

@Component({
    selector: 'locums-examples',
    templateUrl: './examples.component.html',
    styleUrls: ['./examples.component.scss']
})
export class ExamplesComponent extends UnsubscribeOnDestroy implements OnInit {

    readonly testDateForm = this.fb.group(testDateFormConfig);

    today = dayjs.utc().startOf('day');
    datePicked = this.testDateForm.get('datePicked');

    datePicked$ = this.datePicked?.valueChanges.pipe(
        startWith(this.today),
        filter<dayjs.Dayjs>(Boolean),
        map((d) => d.toDate())
    );

    get getDatePicked() {
        return this.datePicked?.value;
    }

    get longAgo() {
        return dayjs(this.datePicked?.value).isBefore(dayjs.utc().subtract(6, 'day'), 'day');
    }
    get onHorizon() {
        return dayjs(this.datePicked?.value).isAfter(dayjs.utc().add(6, 'day'), 'day');
    }

    constructor(
        private dateAdapter: DateAdapter<Dayjs>,
        private fb: FormBuilder
    ) {
        super();
        dayjs.extend(utc);
    }

    ngOnInit(): void {
        this.testDateForm.patchValue({
            datePicked: this.today
        }
        );
    }

    setLocale(locale: string) {
        dayjs.locale(locale);
        this.dateAdapter.setLocale(locale);
    }

    previous() {
        this.datePicked?.patchValue(this.getDatePicked.subtract(1, 'day'));
    }

    next() {
        this.datePicked?.patchValue(this.getDatePicked.add(1, 'day'));
    }
}
