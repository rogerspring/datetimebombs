﻿using DateTimeBombs.BCL.Groceries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DateTimeBombs.BCL
{
    public class Fridge
    {
        public List<IGroceryItem> GroceryItems = new List<IGroceryItem>();
        private readonly IClock _clock;
        private DateTime _getCurrentDate => new DateTime(_clock.GetCurrentTicks(), DateTimeKind.Utc);
        
        public Fridge(IClock clock)
        {
            _clock = clock;
        }

        public void StockFridge(IEnumerable<IGroceryItem> groceryItems)
        {
            GroceryItems.AddRange(groceryItems);
        }

        public bool IsTimeToCleanTheFridge()
        {
            return GroceryItems
                .Any(g => g.ExpirationDate < _getCurrentDate);
        }

        public int NumberOfExpiredItems()
        {
            return GroceryItems
                .Count(g => g.ExpirationDate < _getCurrentDate);
        }
    }
}
