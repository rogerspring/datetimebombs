﻿using System;

namespace DateTimeBombs.BCL
{
    public interface IClock
    {
        long GetCurrentTicks();
    }
}