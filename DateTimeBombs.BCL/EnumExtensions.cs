﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DateTimeBombs.BCL
{
    public  static class EnumExtensions
    {
        public static DateTime GetNextOccurrence(this System.Enum enumFrequency, DateTime date)
        {
            switch (enumFrequency)
            {
                case EnumFrequency.Annually:
                    return date.YearEndDate();
                case EnumFrequency.SemiAnnual:
                    return date.GetSixMonthPeriodEnding();
                case EnumFrequency.Quarterly:
                    return date.GetNextQuarterEndDate();
                case EnumFrequency.Monthly:
                    return date.LastDayOfMonth();
                case EnumFrequency.BiMonthly:
                    return date.GetNextBiMonthlyEndDate();
                case EnumFrequency.Weekly:
                    return date.GetNextWeekEndingDate();
                case EnumFrequency.Daily:
                    return date.GetBeginningOfDay();
                default:
                    throw new InvalidOperationException($"Invalid RedemptionFrequency {System.Enum.GetName(typeof(EnumFrequency), enumFrequency)}");
            }
        }

    }
}
