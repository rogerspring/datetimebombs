﻿using Microsoft.Extensions.Internal;

namespace DateTimeBombs.BCL
{
    public class AppDateTimeProvider : IClock
    {
        private readonly SystemClock _sc;

        private AppDateTimeProvider(){
            _sc = new SystemClock();
        }

        public long GetCurrentTicks()
        {
            return _sc.UtcNow.Ticks;
        }
    }
}
