﻿using DateTimeBombs.BCL.Groceries;
using System;

namespace DateTimeBombs.BCL.Untestables.Groceries
{
    public class Ketchup : GroceryItem, IGroceryItem
    {
        private readonly int _shelfLifeInDays = 182;

        public DateTime ExpirationDate => PurchaseDate.AddDays(_shelfLifeInDays);
        
        public Ketchup()
        {
            PurchaseDate = DateTime.Now;
        }
    }
}
