﻿using DateTimeBombs.BCL.Groceries;
using System;

namespace DateTimeBombs.BCL.Untestables.Groceries
{
    public class Milk : GroceryItem, IGroceryItem
    {
        private readonly int _shelfLifeInDays = 7;

        public DateTime ExpirationDate => PurchaseDate.AddDays(_shelfLifeInDays);
        
        public Milk()
        {
            PurchaseDate = DateTime.Now;
        }
    }
}
