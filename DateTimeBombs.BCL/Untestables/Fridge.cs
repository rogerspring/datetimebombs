﻿using DateTimeBombs.BCL.Groceries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DateTimeBombs.BCL.Untestables
{
    public class Fridge
    {
        public List<IGroceryItem> GroceryItems = new List<IGroceryItem>();
        
        public Fridge()
        {
        }

        public void StockFridge(IEnumerable<IGroceryItem> groceryItems)
        {
            GroceryItems.AddRange(groceryItems);
        }

        public bool IsTimeToCleanTheFridge()
        {
            return GroceryItems
                .Any(g => g.ExpirationDate < DateTime.Now);
        }

        public int NumberOfExpiredItems()
        {
            return GroceryItems
                .Count(g => g.ExpirationDate < DateTime.Now);
        }
    }
}
