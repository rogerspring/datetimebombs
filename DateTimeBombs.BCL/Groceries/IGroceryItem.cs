﻿using System;

namespace DateTimeBombs.BCL.Groceries
{
    public interface IGroceryItem
    {
        DateTime ExpirationDate { get; }
    }
}