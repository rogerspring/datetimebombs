﻿using System;

namespace DateTimeBombs.BCL.Groceries
{
    public class Milk : GroceryItem, IGroceryItem
    {
        private readonly TimeSpan _shelfLife = TimeSpan.FromDays(7);

        public DateTime ExpirationDate => PurchaseDate.Add(_shelfLife);
        
        public Milk(DateTime purchaseDate)
        {
            PurchaseDate = purchaseDate;
        }
    }
}
