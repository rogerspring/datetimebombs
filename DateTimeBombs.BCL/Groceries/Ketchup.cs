﻿using System;

namespace DateTimeBombs.BCL.Groceries
{
    public class Ketchup : GroceryItem, IGroceryItem
    {
        private readonly TimeSpan _shelfLife = TimeSpan.FromDays(182);

        public DateTime ExpirationDate => PurchaseDate.Add(_shelfLife);
        
        public Ketchup(DateTime purchaseDate)
        {
            PurchaseDate = purchaseDate;
        }

    }
}
