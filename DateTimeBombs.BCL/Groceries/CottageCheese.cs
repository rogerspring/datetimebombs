﻿using System;

namespace DateTimeBombs.BCL.Groceries
{
    public class CottageCheese : GroceryItem, IGroceryItem
    {
        private readonly TimeSpan _shelfLife = TimeSpan.FromDays(10);

        public DateTime ExpirationDate => PurchaseDate.Add(_shelfLife);
        
        public CottageCheese(DateTime purchaseDate)
        {
            PurchaseDate = purchaseDate;
        }
    }
}
