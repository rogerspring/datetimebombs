﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;

namespace DateTimeBombs.BCL
{
    public static class DateTimeExtensions
    {

        private static readonly MemoryCache  NextPeriodicDatesUntil_Cache = new MemoryCache(new MemoryCacheOptions());
        private static readonly MemoryCache  NextPeriodicDatesMaxOccurrence_Cache = new MemoryCache(new MemoryCacheOptions());

        /* In all methods, not using AddMonth, because that would
         * rollover to 1 and 2, returning first quarter results
         */

        public static DateTime LastDayOfMonth(this DateTime date)
        {
            var month = date.Month;
            var year = date.Year;

            var lastDayOfMonth = DateTime.DaysInMonth(year, month);
            return DateTime.SpecifyKind(new DateTime(year, month, lastDayOfMonth), date.Kind);

        }

        public static DateTime GetNextBiMonthlyEndDate(this DateTime date)
        {
            var month = date.Month;
            var year = date.Year;

            // There's a more mathematical way of doing this, but for now...
            var endOfMonthOrFifteen = date.Day > 15
                ? Math.Max(date.Day, date.LastDayOfMonth().Day)
                : Math.Max(date.Day, 15);
            
            return DateTime.SpecifyKind(new DateTime(year, month, endOfMonthOrFifteen), date.Kind);

        }

        public static DateTime GetNextWeekEndingDate(this DateTime date, DayOfWeek startDayOfWeek = DayOfWeek.Sunday)
        {
            return date.GetNextWeekday((startDayOfWeek-1) + 7);
        }

        public static DateTime GetNextWeekday(this DateTime start, DayOfWeek day)
        {
            // https://stackoverflow.com/questions/6346119/datetime-get-next-tuesday
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int) day - (int) start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }
        public static DateTime GetPreviousWeekEndingDate(this DateTime date, DayOfWeek startDayOfWeek = DayOfWeek.Sunday)
        {
            return date.GetPreviousWeekday((startDayOfWeek-1) + 7);
        }

        public static DateTime GetPreviousWeekday(this DateTime start, DayOfWeek day)
        {
            // https://stackoverflow.com/questions/6346119/datetime-get-next-tuesday
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int) day - (int) start.DayOfWeek + 7) % 7;
            return start.AddDays(-daysToAdd);
        }

        public static DateTime GetBeginningOfDay(this DateTime date)
        {
            return DateTime.SpecifyKind(new DateTime(date.Year, date.Month, date.Day), date.Kind);
        }

        public static DateTime GetEndOfDay(this DateTime date)
        {
            return DateTime.SpecifyKind(new DateTime(date.Year, date.Month, date.Day).AddTicks(-1).AddDays(1), date.Kind);
        }

        /// <summary>
        /// Note, this is not concerned with Time
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int GetQuarter(this DateTime date)
        {
            return (int) Math.Ceiling(date.Month / 3.0);
        }

        /// <summary>
        /// Note, this is not concerned with Time
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int GetNextQuarter(this DateTime date)
        {
            return (date.GetQuarter() + 1)  % 4;
        }

        /// <summary>
        /// Note, this is not concerned with Time
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetQuarterStartDate(this DateTime date)
        {
            return DateTime.SpecifyKind(new DateTime(date.Year,3*date.GetQuarter()-2,1), date.Kind);
        }

        /// <summary>
        /// Note, this is not concerned with Time
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetQuarterEndDate(this DateTime date)
        {
            return date.GetNextQuarterStartDate().AddDays(-1);
        }

        /// <summary>
        /// Note, this is not concerned with Time
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetNextQuarterStartDate(this DateTime date)
        {
            return DateTime.SpecifyKind(new DateTime(date.Year, date.GetQuarter() * 3, 1).AddMonths(1), date.Kind);
        }

        /// <summary>
        /// Note, this is not concerned with Time
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetNextQuarterEndDate(this DateTime date)
        {
            return date.GetNextQuarterStartDate().AddDays(-1);
        }

        /// <summary>
        /// Note, this is not concerned with Time
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetSixMonthPeriodEnding(this DateTime date)
        {
            return date.Month < 7 ? DateTime.SpecifyKind(new DateTime(date.Year, 6, 30), date.Kind) : date.YearEndDate();
        }

        /// <summary>
        /// Note, this is not concerned with Time
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime YearEndDate(this DateTime date)
        {
            return DateTime.SpecifyKind(new DateTime(date.Year, 12, 31), date.Kind);
        }

        public static DateTime YearBeginDate(this DateTime date)
        {
            return DateTime.SpecifyKind(new DateTime(date.Year, 01, 01), date.Kind);
        }

        public static IEnumerable<DateTime> NextPeriodicDates(this DateTime startDate, EnumFrequency frequency, int maxOccurrences)
        {
            (DateTime startDate, EnumFrequency frequency, int untilDate) key = (startDate, frequency, maxOccurrences);

            if (NextPeriodicDatesMaxOccurrence_Cache.TryGetValue(key, out IEnumerable<DateTime> dates)) return dates;


            var count = 0;
            DateTime previousOccurrence = startDate;
            var nextFrequencyDates = new Collection<DateTime>();

            while (count < maxOccurrences)
            {
                DateTime nextOccurence = frequency.GetNextOccurrence(previousOccurrence);
                
                // Add a day to prevent returning the same occurence each loop.
                // This works until we get to frequencies less than a day.
                previousOccurrence = nextOccurence.AddDays(1);
                
                count++;
                
                nextFrequencyDates.Add(nextOccurence);
            }

            return nextFrequencyDates;
        }

        public static IEnumerable<DateTime> NextPeriodicDates(this DateTime startDate, EnumFrequency frequency, DateTime untilDate)
        {
            (DateTime startDate, EnumFrequency frequency, DateTime untilDate) key = (startDate, frequency, untilDate);

            if (NextPeriodicDatesUntil_Cache.TryGetValue(key, out IEnumerable<DateTime> dates)) return dates;


            DateTime previousOccurrence = startDate;
            var nextFrequencyDates = new Collection<DateTime>();

            while (previousOccurrence < untilDate)
            {
                DateTime nextOccurence = frequency.GetNextOccurrence(previousOccurrence);
                
                // Add a day to prevent returning the same occurence each loop.
                // This works until we get to frequencies less than a day.
                previousOccurrence = nextOccurence.AddDays(1);

                nextFrequencyDates.Add(nextOccurence);
            }
            NextPeriodicDatesUntil_Cache.Set(key, nextFrequencyDates);
            return nextFrequencyDates;
        }

        public static int NumberOfPeriodicOccurrencesUntil(this DateTime startDate, EnumFrequency frequency,
            DateTime untilDate)
        {
            return startDate.NextPeriodicDates(frequency, untilDate).ToArray().Length;
        }
    }
}
