﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeZoneNames;

namespace DateTimeBombs.BCL
{
    public static class DateTimeZoneConverter
    {
        public static string ExtractTimeZoneName(string dateTimeZoneString)
        {
            IDictionary<string, string> tzs = TZNames.GetDisplayNames("en-US");
            var tz = tzs.Keys.Concat(new[] {"Z"}).FirstOrDefault(dateTimeZoneString.EndsWith);
            tz = tz == "Z" ? "UTC" : tz;
            return tz;
        }

        public static DateTimeOffset ToDateTimeOffset(string dateTimeZoneString)
        {
            var tzName = ExtractTimeZoneName(dateTimeZoneString);

            if (string.IsNullOrWhiteSpace(tzName) || tzName == "UTC")
            {
                return DateTimeOffset.Parse(dateTimeZoneString);
            }

            var dtString = dateTimeZoneString.Replace(tzName, "").Trim();
            var dateTime = DateTime.SpecifyKind(DateTime.Parse(dtString), DateTimeKind.Unspecified);
            var offset = TimeZoneInfo.FindSystemTimeZoneById(tzName).GetUtcOffset(dateTime);

            return new DateTimeOffset(dateTime, offset);
        }
    }
}
