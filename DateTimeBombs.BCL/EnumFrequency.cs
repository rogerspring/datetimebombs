﻿namespace DateTimeBombs.BCL
{
    public enum EnumFrequency
    {
        Annually,
        SemiAnnual,
        Quarterly,
        Monthly,
        BiMonthly,
        Weekly,
        Daily
    }
}