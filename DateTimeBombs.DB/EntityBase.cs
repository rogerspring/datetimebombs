﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using NodaTime;

namespace DateTimeBombs.DB
{
    public abstract class EntityBase : ICreatedModified
    {
        public Instant? Created { get; set; }
        public Instant? Modified { get; set; }
        [MaxLength(255)]
        public string CreatedBy { get; set; }
        [MaxLength(255)]
        public string ModifiedBy { get; set; }

        protected static void AttachToContext<TEntity>(TEntity target, DbContext context)
            where TEntity: EntityBase, IIdentified<int>
        {
            EntityEntry entity = context.Attach(target);
            entity.State = target.Id > 0 ? EntityState.Modified : EntityState.Added;
        }
    }
}
