﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.SqlServer.Storage;
using NodaTime;

namespace DateTimeBombs.DB
{
    /// <summary>
    /// This depends on the nuget package SimplerSoftware.EntityFrameworkCore.SqlServer.NodaTime, 
    /// </summary>
    public static class PropertyBuilderExtensions
    {

        public static void ConfigureAsLocalDate(this PropertyBuilder<LocalDateTime> builder)
        {
            builder
                .HasConversion(new LocalDateTimeValueConverter())
                .HasColumnType("datetime2");
        }

        public static void ConfigureAsLocalDate(this PropertyBuilder<LocalDateTime?> builder)
        {
            builder
                .HasConversion(new LocalDateTimeValueConverter())
                .HasColumnType("datetime2");
        }

        public static void ConfigureAsDuration(this PropertyBuilder<Duration> builder)
        {
            builder
                .HasConversion(new DurationValueConverter())
                .HasColumnType("time");
        }

        public static void ConfigureAsDuration(this PropertyBuilder<Duration?> builder)
        {
            builder
                .HasConversion(new DurationValueConverter())
                .HasColumnType("time");
        }

        public static void ConfigureAsLocalDate(this PropertyBuilder<LocalDate> builder)
        {
            builder
                .HasConversion(new LocalDateValueConverter())
                .HasColumnType("date");
        }

        public static void ConfigureAsLocalDate(this PropertyBuilder<LocalDate?> builder)
        {
            builder
                .HasConversion(new LocalDateValueConverter())
                .HasColumnType("date");
        }

        public static void ConfigureAsOffsetDateTime(this PropertyBuilder<OffsetDateTime?> builder)
        {
            builder
                .HasConversion(new OffsetDateTimeValueConverter())
                .HasColumnType("datetimeoffset");
        }

        public static void ConfigureAsOffsetDateTime(this PropertyBuilder<OffsetDateTime> builder)
        {
            builder
                .HasConversion(new OffsetDateTimeValueConverter())
                .HasColumnType("datetimeoffset");
        }

        public static void ConfigureAsInstant(this PropertyBuilder<Instant> builder)
        {
            builder
                .HasConversion(new InstantValueConverter())
                .HasColumnType("datetime2");
        }

        public static void ConfigureAsInstant(this PropertyBuilder<Instant?> builder)
        {
            builder
                .HasConversion(new InstantValueConverter())
                .HasColumnType("datetime2");
        }

        public static void ConfigureAsLocalTime(this PropertyBuilder<LocalTime> builder)
        {
            builder
                .HasConversion(new LocalTimeValueConverter())
                .HasColumnType("time");
        }

        public static void ConfigureAsLocalTime(this PropertyBuilder<LocalTime?> builder)
        {
            builder
                .HasConversion(new LocalTimeValueConverter())
                .HasColumnType("time");
        }
    }
}

