﻿using NodaTime;

namespace DateTimeBombs.DB
{
    public interface ICreatedModified
    {
        Instant? Created { get; set; }
        Instant? Modified { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
    }
}
