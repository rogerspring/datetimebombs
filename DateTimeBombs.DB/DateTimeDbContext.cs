﻿using DateTimeBombs.DB.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.NodaTime.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DateTimeBombs.DB
{
    public class DateTimeDbContext : DbContext
    {
        private static readonly ILoggerFactory DbLoggerFactory = LoggerFactory.Create(builder => { builder.AddDebug(); });

        public DateTimeDbContext(IConfiguration configuration) : base(GetOptions(configuration)) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//#if DEBUG
            optionsBuilder.UseLoggerFactory(DbLoggerFactory);
//#endif
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DateTimeDbContext).Assembly);

        }

        private static DbContextOptions<DateTimeDbContext> GetOptions(IConfiguration configuration)
        {
            return new DbContextOptionsBuilder<DateTimeDbContext>()
                .UseSqlServer(
                    connectionString: configuration.GetConnectionString("DefaultConnection")
                    , sqlServerOptionsAction: b => b.UseNodaTime()
                ).Options;
        }

        public DbSet<GroceryItem> GroceryItems { get; set; }
    }
}
