﻿using AutoBogus;
using DateTimeBombs.DB.Entities;
using EFCore.BulkExtensions;
using Microsoft.Extensions.Configuration;
using NodaTime;

namespace DateTimeBombs.DB
{
    public class Seeder
    {
        private readonly IConfiguration _configuration;

        public Seeder(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string SeedMeSeymour()
        {
            #region Skip NodaTime types
            AutoFaker.Configure(b =>
                b
                    .WithSkip(typeof(LocalDate?))
                    .WithSkip(typeof(LocalDate))
                    .WithSkip(typeof(OffsetDateTime?))
                    .WithSkip(typeof(OffsetDateTime))
                    .WithSkip(typeof(Instant?))
                    .WithSkip(typeof(Instant))
                    .WithSkip(typeof(Duration?))
                    .WithSkip(typeof(Duration))
                    .WithSkip(typeof(Interval?))
                    .WithSkip(typeof(Interval))
                    .WithSkip(typeof(LocalDateTime?))
                    .WithSkip(typeof(LocalDateTime))
                    .WithSkip(typeof(ZonedDateTime?))
                    .WithSkip(typeof(ZonedDateTime))
                    .WithSkip(typeof(YearMonth?))
                    .WithSkip(typeof(YearMonth))
                    .WithSkip(typeof(OffsetTime?))
                    .WithSkip(typeof(OffsetTime))
                    .WithSkip(typeof(Offset?))
                    .WithSkip(typeof(Offset))
                    .WithSkip(typeof(LocalTime?))
                    .WithSkip(typeof(LocalTime))
                    .WithSkip(typeof(OffsetDate?))
                    .WithSkip(typeof(OffsetDate))
                    .WithSkip(typeof(AnnualDate?))
                    .WithSkip(typeof(AnnualDate))
                );
            #endregion
            var groceryGenerator = new AutoFaker<GroceryItem>()
                    .RuleFor(g => g.Id, f => f.IndexFaker)
                    .RuleFor(g => g.Created, f => Instant.FromDateTimeOffset(f.Date.Past()))
                    .RuleFor(g => g.Modified, (f, g) => g.Created?.Plus(Duration.FromDays(f.Random.Number(40))))
                    .RuleFor(g => g.ExpirationDate, (f, g) => g.Created.HasValue ? LocalDate.FromDateTime(g.Created.Value.ToDateTimeOffset().DateTime) : (LocalDate?) null)
                    .RuleFor(g => g.SomeOtherDate, (f, g) => OffsetDateTime.FromDateTimeOffset(f.Date.PastOffset()))
                ;
            
            var oldGroceries = groceryGenerator
                .Generate(30);
            var newGroceries = groceryGenerator
                .RuleFor(g => g.ExpirationDate, (f, g) => g.Created.HasValue ? LocalDate.FromDateTime(g.Created.Value.ToDateTimeOffset().DateTime) : (LocalDate?) null)
                .RuleFor(g => g.SomeOtherDate, (f, g) => OffsetDateTime.FromDateTimeOffset(f.Date.FutureOffset()))
                .Generate(30);
            var nullCreationGroceries = groceryGenerator
                .RuleFor(g => g.ExpirationDate, (f, g) => g.Created.HasValue ? LocalDate.FromDateTime(g.Created.Value.ToDateTimeOffset().DateTime) : (LocalDate?) null)
                .RuleFor(g => g.SomeOtherDate, (f, g) => OffsetDateTime.FromDateTimeOffset(f.Date.FutureOffset()))
                .FinishWith((f, g) => g.Created = null)
                .Generate(2)
                ;


            var db = new DateTimeDbContext(_configuration);
            db.BulkInsert(nullCreationGroceries);
            db.BulkInsert(oldGroceries);
            db.BulkInsert(newGroceries);
            db.SaveChanges();
            
            return @"
             ___ ___  ___ ___  ___ ___  ___ ___                                     
            |   |   ||   |   ||   |   ||   |   |                                    
            | _   _ || _   _ || _   _ || _   _ |                                    
            |  \_/  ||  \_/  ||  \_/  ||  \_/  |                                    
            |   |   ||   |   ||   |   ||   |   |                                    
            |   |   ||   |   ||   |   ||   |   |                                    
            |___|___||___|___||___|___||___|___|                                    
                                                                                    
             ______  __ __   ____  ______  __  _____      ____   ___    ___   ___   
            |      ||  |  | /    ||      ||  |/ ___/     /    | /   \  /   \ |   \  
            |      ||  |  ||  o  ||      ||_ (   \_     |   __||     ||     ||    \ 
            |_|  |_||  _  ||     ||_|  |_|  \|\__  |    |  |  ||  O  ||  O  ||  D  |
              |  |  |  |  ||  _  |  |  |      /  \ |    |  |_ ||     ||     ||     |
              |  |  |  |  ||  |  |  |  |      \    |    |     ||     ||     ||     |
              |__|  |__|__||__|__|  |__|       \___|    |___,_| \___/  \___/ |_____|
                                                                                    
            ";
        }
    }
}
