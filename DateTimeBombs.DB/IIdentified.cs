﻿namespace DateTimeBombs.DB
{
    public interface IIdentified<TKey>
    {
        TKey Id { get; set; }
    }
}
