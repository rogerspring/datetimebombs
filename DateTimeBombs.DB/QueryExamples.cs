﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateTimeBombs.DB.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.NodaTime.Extensions;
using Microsoft.Extensions.Configuration;
using NodaTime;
using NodaTime.Extensions;

namespace DateTimeBombs.DB
{
    public class QueryExamples
    {
        private readonly IConfiguration _configuration;
        private Instant _now;

        public QueryExamples(IConfiguration configuration)
        {
            _configuration = configuration;
            _now = SystemClock.Instance.GetCurrentInstant();
        }

        public IEnumerable<GroceryItem> GetNonExpired()
        {
            var db = new DateTimeDbContext(_configuration);
            LocalDate today = _now.ToDateTimeOffset().ToOffsetDateTime().Date;
            return db.GroceryItems.Where(g => g.ExpirationDate > today).ToArray();
            /****** Works out of the box ******/

            /*
                exec sp_executesql N'SELECT [g].[GroceryItemId], [g].[Created], [g].[CreatedBy], [g].[ExpirationDate], [g].[Modified], [g].[ModifiedBy], [g].[Name], [g].[SomeOtherDate]
                FROM [GroceryItems] AS [g]
                WHERE [g].[ExpirationDate] > @__today_0',N'@__today_0 date',@__today_0='2020-12-02'
            */
        }

        public IEnumerable<GroceryItem> GetByCreationInstant()
        {
            var db = new DateTimeDbContext(_configuration);
            return db.GroceryItems.Where(g => g.Created <= _now.PlusDays(-40)).ToList();
            /****** Works out of the box ******/
    
            /*
                exec sp_executesql N'SELECT [g].[GroceryItemId], [g].[Created], [g].[CreatedBy], [g].[ExpirationDate], [g].[Modified], [g].[ModifiedBy], [g].[Name], [g].[SomeOtherDate]
                FROM [GroceryItems] AS [g]
                WHERE [g].[Created] <= @__PlusDays_0',N'@__PlusDays_0 datetimeoffset(7)',@__PlusDays_0='2020-10-23 15:04:59.1872121 +00:00'
            */
        }

        public IEnumerable<GroceryItem> GetByCreationInstantIsNull()
        {
            var db = new DateTimeDbContext(_configuration);
            return db.GroceryItems.Where(g => !g.Created.HasValue).ToList();
            /****** Works out of the box ******/

            /*
                SELECT [g].[GroceryItemId], [g].[Created], [g].[CreatedBy], [g].[ExpirationDate], [g].[Modified], [g].[ModifiedBy], [g].[Name], [g].[SomeOtherDate]
                FROM [GroceryItems] AS [g]
                WHERE [g].[Created] IS NULL
            */
        }

        public IEnumerable<GroceryItem> GetBySomeOtherFutureDate()
        {
            var db = new DateTimeDbContext(_configuration);
            DbFunctions dbFunctions = null;
            OffsetDateTime? now = OffsetDateTime.FromDateTimeOffset(_now.ToDateTimeOffset());
            return db.GroceryItems.Where(g => dbFunctions.DateDiffMinute(g.SomeOtherDate, now) > 0).ToList();
            /****** Exception '...could not be translated...' out of the box ******/
            
            /****** With nuget package *******
                exec sp_executesql N'SELECT [g].[GroceryItemId], [g].[Created], [g].[CreatedBy], [g].[ExpirationDate], [g].[Modified], [g].[ModifiedBy], [g].[Name], [g].[SomeOtherDate]
                FROM [GroceryItems] AS [g]
                WHERE DATEDIFF(SECOND, [g].[SomeOtherDate], @__now_1) > 0',N'@__now_1 datetimeoffset(7)',@__now_1='2020-12-02 15:11:55.9938253 +00:00'            */
        }

        public IEnumerable<GroceryItem> GetBySomeOtherPastDate()
        {
            var db = new DateTimeDbContext(_configuration);
            DbFunctions dbFunctions = null;
            OffsetDateTime? now = OffsetDateTime.FromDateTimeOffset(_now.ToDateTimeOffset());
            return db.GroceryItems.Where(g => dbFunctions.DateDiffSecond(g.SomeOtherDate, now) < 0).ToList();
            /****** Exception '...could not be translated...' out of the box ******/
            
            /****** With nuget package *******
                exec sp_executesql N'SELECT [g].[GroceryItemId], [g].[Created], [g].[CreatedBy], [g].[ExpirationDate], [g].[Modified], [g].[ModifiedBy], [g].[Name], [g].[SomeOtherDate]
                FROM [GroceryItems] AS [g]
                WHERE DATEDIFF(SECOND, [g].[SomeOtherDate], @__now_1) < 0',N'@__now_1 datetimeoffset(7)',@__now_1='2020-12-02 15:12:11.7112352 +00:00'            
            */
        }
    }
}
