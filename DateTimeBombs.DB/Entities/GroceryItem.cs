﻿using AutoBogus;
using Bogus;
using DateTimeBombs.NodaTimeDateTimeProvider.Groceries;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NodaTime;

namespace DateTimeBombs.DB.Entities
{
    public class GroceryItem: EntityBase, IGroceryItem, IIdentified<int>
    {
        public int Id { get; set; }
        public LocalDate ExpirationDate { get; set; }
        public OffsetDateTime SomeOtherDate { get; set; }
        public string Name { get; set; }
    }

    public class GroceryItemConfig
    {
        internal class AddressConfiguration : IEntityTypeConfiguration<GroceryItem>
        {
            public void Configure(EntityTypeBuilder<GroceryItem> builder)
            {
                builder
                    .HasKey(e => e.Id);

                builder
                    .Property(e => e.Id)
                    .HasColumnName("GroceryItemId");

                builder.Property(e => e.ExpirationDate)
                    .ConfigureAsLocalDate();

                builder.Property(e => e.Created)
                    .HasColumnType("datetime2");

                builder.Property(e => e.CreatedBy)
                    .HasColumnType("nvarchar(255)")
                    .HasMaxLength(255);

                builder.Property(e => e.Modified)
                    .HasColumnType("datetime2");

                builder.Property(e => e.SomeOtherDate)
                    .HasColumnType("datetimeoffset");


                builder.Property(e => e.ModifiedBy)
                    .HasColumnType("nvarchar(255)")
                    .HasMaxLength(255);

                builder
                    .Property(e => e.Created)
                    .ConfigureAsInstant();

                builder
                    .Property(e => e.Modified)
                    .ConfigureAsInstant();

                builder
                    .Property(e => e.SomeOtherDate)
                    .ConfigureAsOffsetDateTime();
            }
        }
    }
}
