﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DateTimeBombs.DB
{
    public static class IEnumerableExtensions
    {
        public static void ForEach<TItem>(this IEnumerable<TItem> source, Action<TItem> action)
        {
            if (source != null)
            {
                source.ToArray().ToList().ForEach(action);
            }
        }

    }
}
